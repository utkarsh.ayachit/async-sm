#include "renderviewproxy.h"

#include <vtkCamera.h>
#include <vtkCommand.h>
#include <vtkDataSetReader.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkImageData.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkTexture.h>
#include <vtkWeakPointer.h>

#include <comm/loop_executor.h>
#include <comm/service.h>
#include <sm/client/session.h>
#include <sm/common/proxy.pb.h>
#include <sm/server/vtkextensions.h>
#include <stlab/concurrency/default_executor.hpp>
#include <string>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <atomic>
#include <chrono>
#include <thread>

namespace sm
{
namespace client
{

class RenderViewProxy::RVPInternals
{
public:
  vtkNew<vtkGenericOpenGLRenderWindow> Window;
  vtkNew<vtkRenderer> Renderer;
  vtkWeakPointer<vtkRenderWindowInteractor> Interactor;
  unsigned long InteractorObserverId;

  vtkNew<vtkTexture> Texture;

  stlab::receiver<void> RenderingReceiver;

  RVPInternals()
    : Token(0)
  {
    this->Window->AddRenderer(this->Renderer);
    this->Renderer->SetBackgroundTexture(this->Texture);
    this->Renderer->ResetCamera(-1, 1, -1, 1, -1, 1);
  }
  ~RVPInternals()
  {
    if (this->Interactor)
    {
      this->Interactor->RemoveObserver(this->InteractorObserverId);
    }
  }

  void Push(vtkSmartPointer<vtkImageData> img, const std::uint64_t token)
  {
    if (this->Token == token + 1)
    {
      LOG_S(1) << "render image";
      this->Texture->SetInputDataObject(img);
      this->Renderer->TexturedBackgroundOn();
      this->Window->Render();
      // using namespace std::chrono_literals;
      // std::this_thread::sleep_for(0.1s);
    }
    else
    {
      LOG_S(1) << "discard image";
    }
  }

  std::atomic<std::uint64_t> Token;
};

namespace detail
{
}

//-----------------------------------------------------------------------------
RenderViewProxy::RenderViewProxy(std::weak_ptr<sm::client::Session> session)
  : Proxy(session)
  , Internals(new RenderViewProxy::RVPInternals())
  , Channel("render." + std::to_string(this->GetState().global_id()))
{
  this->SetLocations({ "ds", "rs" });
  auto& internals = *this->Internals;

  this->GetState().set_xml_group("views");
  this->GetState().set_xml_name("RenderView");

  if (auto s = session.lock())
  {
    auto executor = comm::loop_executor_type{};
    auto recv = s->GetService().subscribe(this->Channel);
    internals.RenderingReceiver = recv |
      stlab::executor(stlab::default_executor) &
        [&internals, executor](std::shared_ptr<const comm::Message> msg) {
          assert(msg->size() == 1);
          LOG_S(1) << "got data to render." << msg->get_debug_string();
          auto obj = server::vtkextensions::extract_vtk_object(msg->at(0));
          vtkSmartPointer<vtkImageData> img = vtkImageData::SafeDownCast(obj);

          auto token = internals.Token++;
          auto f = [&internals, token, img]() { internals.Push(img, token); };
          executor.enqueue_on_idle(f);
        };
    recv.set_ready();
  }
}

//-----------------------------------------------------------------------------
RenderViewProxy::~RenderViewProxy()
{
}

//-----------------------------------------------------------------------------
vtkGenericOpenGLRenderWindow* RenderViewProxy::GetRenderWindow() const
{
  return this->Internals->Window;
}

//-----------------------------------------------------------------------------
void RenderViewProxy::SetupInteractor(vtkRenderWindowInteractor* iren)
{
  auto& internals = *this->Internals;
  assert(internals.Interactor == nullptr && iren != nullptr);

  internals.Interactor = iren;
  internals.InteractorObserverId =
    iren->AddObserver(vtkCommand::RenderEvent, this, &RenderViewProxy::Render);

  // Turn off direct rendering from the interactor. The interactor calls
  // vtkRenderWindow::Render(). We don't want that. We want it to go through
  // the vtkSMViewProxy layer.
  iren->EnableRenderOff();
}

//-----------------------------------------------------------------------------
void RenderViewProxy::Update()
{
  sm::common::UpdatePipeline up;
  up.set_time(1.0);
  this->SendGenericRequest(up, { "ds" });
}

//-----------------------------------------------------------------------------
void RenderViewProxy::ResetCamera()
{
  sm::common::Variant var;
  var.set_str("GetPropBounds");
  this->SendGenericRequestWithResponse(var, { "rs" })
    .front()
    .then(comm::loop_executor_type{},
      [this](const sm::common::ProxyGenericResponse& response) {
        if (response.body().Is<sm::common::Bounds>())
        {
          sm::common::Bounds bds;
          response.body().UnpackTo(&bds);
          double dbds[6]{ bds.xmin(), bds.xmax(), bds.ymin(), bds.ymax(), bds.zmin(), bds.xmax() };
          this->Internals->Renderer->ResetCamera(dbds);
          this->Render();
        }
      })
    .detach();
}

//-----------------------------------------------------------------------------
void RenderViewProxy::Azimuth(double angle)
{
  auto camera = this->Internals->Renderer->GetActiveCamera();
  camera->Azimuth(angle);
  camera->OrthogonalizeViewUp();
  this->Render();
}

//-----------------------------------------------------------------------------
void RenderViewProxy::Render()
{
  auto camera = this->Internals->Renderer->GetActiveCamera();
  sm::common::Render render;
  if (auto pos = render.mutable_position())
  {
    pos->set_x(camera->GetPosition()[0]);
    pos->set_y(camera->GetPosition()[1]);
    pos->set_z(camera->GetPosition()[2]);
  }
  if (auto fp = render.mutable_focal_point())
  {
    fp->set_x(camera->GetFocalPoint()[0]);
    fp->set_y(camera->GetFocalPoint()[1]);
    fp->set_z(camera->GetFocalPoint()[2]);
  }
  if (auto vu = render.mutable_view_up())
  {
    vu->set_x(camera->GetViewUp()[0]);
    vu->set_y(camera->GetViewUp()[1]);
    vu->set_z(camera->GetViewUp()[2]);
  }
  if (auto size = render.mutable_size())
  {
    size->set_x(this->Internals->Window->GetSize()[0]);
    size->set_y(this->Internals->Window->GetSize()[1]);
  }
  this->SendGenericRequest(render, { "rs" });
  LOG_S(1) << "request::Render";

  // always render whatever we have.
  this->Internals->Window->Render();
}
}
}
