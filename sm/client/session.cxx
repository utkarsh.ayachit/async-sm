#include "session.h"

#include <comm/service.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/utility.hpp>

namespace sm
{
namespace client
{

class Session::SInternals
{
public:
  comm::Service Service;

  SInternals()
    : Service("client")
  {
  }
};

//-----------------------------------------------------------------------------
Session::Session()
{
}

//-----------------------------------------------------------------------------
Session::~Session()
{
}

//-----------------------------------------------------------------------------
bool Session::Initialize(const std::string& brokerURL)
{
  auto& internals = this->Internals;
  internals.reset(new Session::SInternals());
  if (!stlab::blocking_get(internals->Service.connect(brokerURL)))
  {
    internals.reset();
    return false;
  }

  return true;
}

//-----------------------------------------------------------------------------
const comm::Service& Session::GetService() const
{
  return this->Internals->Service;
}
}
}
