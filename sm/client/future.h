#ifndef sm_client_future_h
#define sm_client_future_h

#include <stlab/concurrency/future.hpp>
#include <stlab/concurrency/utility.hpp>

namespace sm
{
namespace client
{
template <typename T>
class future
{
public:
  future(stlab::future<T>&& f)
    : _f(f)
  {
  }
  ~future() { stlab::blocking_get(_f); }
private:
  stlab::future<T> _f;
};
}
}
#endif
