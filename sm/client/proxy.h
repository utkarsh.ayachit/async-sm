#ifndef sm_client_proxy_h
#define sm_client_proxy_h

#include <memory>
#include <string>
#include <vector>

#include <comm/internal/serialization_pb.h>
#include <comm/message.h>
#include <sm/client/future.h>
#include <sm/common/proxy.pb.h>

namespace sm
{
namespace client
{
class Session;

/**
 * @class Proxy
 * @brief mockup of a SMProxy.
 *
 * This is simply a standin for vtkSMProxy.
 */
class Proxy
{
public:
  Proxy(std::weak_ptr<sm::client::Session> session);
  ~Proxy();

  std::uint64_t GetGlobalID() const;

  void SetLocations(const std::vector<std::string>& locs) { this->Locations = locs; }
  const std::vector<std::string> GetLocations() const { return this->Locations; }

  // for now just expose the state
  sm::common::ProxyState& GetState() { return this->State; }

  void UpdateVTKObjects();
  void UpdatePipeline(double time);

  std::shared_ptr<Session> GetSession() const { return this->SessionObj.lock(); }

  //@{
  /**
   * arbitrary convenience methods
   */
  void setInput(std::shared_ptr<Proxy> proxy);
  std::shared_ptr<Proxy> input() const { return this->Input; }
  //@}

  enum KnownProxies
  {
    SPHERE,
    WAVELET,
    CONTOUR,
    LIVE,
    SENSEILIVE,
    REPRESENTATION_GEOMETRY,
    SLICE,
  };

  /**
   * stand-in for proxy manager to create a proxy of specific type.
   */
  static std::shared_ptr<Proxy> createProxy(
    KnownProxies type, std::shared_ptr<sm::client::Session> session);

  //@{
  /**
   * api to set icon.
   */
  void SetIcon(const std::string& name) { this->Icon = name; }
  const std::string& GetIcon() const { return this->Icon; }
  //@}

  stlab::future<std::string> GetDataInformation();

protected:
  /**
   * Send a ProxyGenericRequest with the given body.
   */
  void SendGenericRequest(const google::protobuf::Message& body,
    const std::vector<std::string>& locs = std::vector<std::string>());

  std::vector<stlab::future<sm::common::ProxyGenericResponse> > SendGenericRequestWithResponse(
    const google::protobuf::Message& body,
    const std::vector<std::string>& locs = std::vector<std::string>());

private:
  Proxy(const Proxy&) = delete;
  void operator=(const Proxy&) = delete;

  std::weak_ptr<Session> SessionObj;
  std::vector<std::string> Locations;
  sm::common::ProxyState State;
  std::shared_ptr<Proxy> Input;
  std::string Icon;
};
}
}

#endif
