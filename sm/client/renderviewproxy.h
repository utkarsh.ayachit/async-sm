#ifndef sm_client_renderviewproxy_h
#define sm_client_renderviewproxy_h

#include "proxy.h"

class vtkGenericOpenGLRenderWindow;
class vtkRenderWindowInteractor;

namespace sm
{
namespace client
{
class RenderViewProxy : public Proxy
{
public:
  RenderViewProxy(std::weak_ptr<sm::client::Session> session);
  ~RenderViewProxy();

  vtkGenericOpenGLRenderWindow* GetRenderWindow() const;
  void SetupInteractor(vtkRenderWindowInteractor* iren);

  /// request an update.
  void Update();

  /// request a render.
  void Render();

  /// request reset camera.
  void ResetCamera();

  void Azimuth(double angle);

private:
  RenderViewProxy(const RenderViewProxy&) = delete;
  void operator=(const RenderViewProxy&) = delete;

  class RVPInternals;
  std::unique_ptr<RVPInternals> Internals;
  std::string Channel;
};
}
}

#endif
