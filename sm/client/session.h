#ifndef sm_client_session_h
#define sm_client_session_h

#include <memory>
#include <string>
namespace comm
{
class Service;
}
namespace sm
{
namespace client
{

class Session
{
public:
  Session();
  ~Session();

  bool Initialize(const std::string& brokerURL);

  const comm::Service& GetService() const;

private:
  Session(const Session&) = delete;
  void operator=(const Session&) = delete;

  class SInternals;
  std::unique_ptr<SInternals> Internals;
};
}
}

#endif
