#ifndef sm_server_proxyhandler_h
#define sm_server_proxyhandler_h

#include <memory>

namespace comm
{
class Message;
}

namespace sm
{
namespace server
{

class Session;
class Proxy;

/**
 * @class ProxyHandler
 * @brief handles/routes messages sent by sm::client::Proxy and related classes.
 *
 * ProxyHandler handles messages sent by sm::client::Proxy and related classes.
 */
class ProxyHandler
{
public:
  ProxyHandler(const Session& session);
  ~ProxyHandler();

  comm::Message Preview(std::uint64_t msg_num, comm::Message&& msg);
  void Process(std::uint64_t msg_num, comm::Message&& msg);

  const Session& GetSession() const;
  std::shared_ptr<Proxy> LocateProxy(uint64_t gid) const;

private:
  ProxyHandler(const ProxyHandler&) = delete;
  void operator=(const ProxyHandler&) = delete;

  class PHInternals;
  std::unique_ptr<PHInternals> Internals;
};
}
}
#endif
