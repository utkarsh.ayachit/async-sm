#include "sourceproxy.h"

#include <vtkAlgorithm.h>
#include <vtkCommand.h>
#include <vtkCompositeDataIterator.h>
#include <vtkCompositeDataSet.h>
#include <vtkNew.h>
#include <vtkObject.h>
#include <vtkSmartPointer.h>

#include <comm/internal/serialization_pb.h>
#include <comm/message.h>
#include <comm/service.h>
#include <sm/server/session.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>
namespace sm
{
namespace server
{

namespace detail
{
vtkIdType elementCount(vtkDataObject* dobj, int type)
{
  if (auto cd = vtkCompositeDataSet::SafeDownCast(dobj))
  {
    vtkIdType count{ 0 };
    auto iter = cd->NewIterator();
    for (iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
    {
      count += elementCount(iter->GetCurrentDataObject(), type);
    }
    iter->Delete();
    return count;
  }
  else
  {
    return dobj->GetNumberOfElements(type);
  }
}

std::string dataInformation(vtkObject* obj)
{
  if (auto algo = vtkAlgorithm::SafeDownCast(obj))
  {
    if (auto dobj = algo->GetOutputDataObject(0))
    {
      std::ostringstream str;
      str.imbue(std::locale(""));
      str << "<p><b>data type</b>: " << dobj->GetClassName() << "</p>";
      str << "<p><b>number of cells</b>: " << std::fixed << elementCount(dobj, vtkDataObject::CELL)
          << "</p>";
      str << "<p><b>number of points</b>: " << std::fixed
          << elementCount(dobj, vtkDataObject::POINT) << "</p>";
      return str.str();
    }
  }
  return "";
}
}

//-----------------------------------------------------------------------------
SourceProxy::SourceProxy(vtkAlgorithm* obj, const Session& session)
  : Superclass(obj, session)
{
}

//-----------------------------------------------------------------------------
SourceProxy::~SourceProxy()
{
}

//-----------------------------------------------------------------------------
vtkAlgorithm* SourceProxy::GetVTKAlgorithm() const
{
  return vtkAlgorithm::SafeDownCast(this->GetVTKObject());
}

//-----------------------------------------------------------------------------
bool SourceProxy::Preview(std::uint64_t msg_num,
  const comm::Message& msg,
  const sm::common::ProxyGenericRequest& req)
{
  if (req.body().Is<sm::common::ProxyState>())
  {
    // save the msg tag that is going to invalidate this object.
    // MakeDirtyMsgTag is atomic, hence thread safe.
    this->MakeDirtyMsgTag = msg_num;
    LOG_S(INFO) << "preview dirty: " << msg_num;
  }
  return this->Superclass::Preview(msg_num, msg, req);
}

//-----------------------------------------------------------------------------
void SourceProxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->Superclass::UpdateState(pstate);
  // TODO: only update things that change.
  if (auto algo = this->GetVTKAlgorithm())
  {
    for (const auto& pair : pstate.properties())
    {
      if (pair.first == "Input" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kProxy)
      {
        auto pid = pair.second.values(0).proxy();
        if (auto iproxy = this->LocateProxy(pid))
        {
          auto ialgo = vtkAlgorithm::SafeDownCast(iproxy->GetVTKObject());
          algo->SetInputConnection(ialgo->GetOutputPort());
          LOG_S(INFO) << "setting input: " << ialgo->GetClassName() << " (" << ialgo << ") --> "
                      << algo->GetClassName() << " (" << algo << ")";
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------
bool SourceProxy::Process(std::uint64_t msg_num,
  const comm::Message& msg,
  const sm::common::ProxyGenericRequest& req)
{
  if (this->Superclass::Process(msg_num, msg, req))
  {
    return true;
  }

  auto& service = this->GetSession().GetService();
  if (req.body().Is<sm::common::UpdatePipeline>())
  {
    if (msg_num >= this->MakeDirtyMsgTag)
    {
      sm::common::UpdatePipeline up;
      req.body().UnpackTo(&up);
      this->UpdatePipeline(up.time());
      service.send_reply(msg, comm::Message());
    }
    else
    {
      LOG_S(INFO) << "ignoring obsolete UpdatePipeline";
      // TODO: send reply indicating the  request was "cancelled".
      service.send_reply(msg, comm::Message());
    }
    return true;
  }
  else if (req.body().Is<sm::common::GatherInformation>())
  {
    if (msg_num >= this->MakeDirtyMsgTag)
    {
      sm::common::GatherInformation ginfo;
      req.body().UnpackTo(&ginfo);
      if (ginfo.type() == "DataInformation")
      {
        sm::common::DataInformation dinfo;
        if (auto vtkobject = this->GetVTKObject())
        {
          std::ostringstream str;
          str << "<html><body>"
              << "<p><b>class</b>: " << vtkobject->GetClassName() << "</p>"
              << detail::dataInformation(vtkobject) << "</body></html>";
          dinfo.set_text(str.str());
        }
        sm::common::ProxyGenericResponse response;
        response.mutable_body()->PackFrom(dinfo);
        comm::Message replyMsg;
        replyMsg.push_back(response);
        service.send_reply(msg, std::move(replyMsg));
      }
    }
    else
    {
      // TODO: send reply indicating the  request was "cancelled".
      LOG_S(INFO) << "ignoring obsolete DataInformation";
      sm::common::ProxyGenericResponse response;
      comm::Message replyMsg;
      replyMsg.push_back(response);
      service.send_reply(msg, std::move(replyMsg));
    }
    return true;
  }
  return false;
}

//-----------------------------------------------------------------------------
void SourceProxy::UpdatePipeline(double time)
{
  if (auto algo = this->GetVTKAlgorithm())
  {
    algo->Update();
  }
}
}
}
