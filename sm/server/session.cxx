#include "session.h"

#include <comm/loop_executor.h>
#include <comm/zmq.h>
#include <sm/common/proxy.pb.h>
#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/default_executor.hpp>
#include <stlab/concurrency/utility.hpp>

#include "proxyhandler.h"

namespace sm
{
namespace server
{

struct PreviewProcess
{
  std::weak_ptr<ProxyHandler> PHandler;
  std::pair<std::uint64_t, comm::Message> LastMessagePair;
  stlab::process_state_scheduled State = stlab::await_forever;
  std::shared_ptr<std::atomic<std::uint64_t> > Counter{ 0 };

public:
  PreviewProcess(std::weak_ptr<ProxyHandler> handler)
    : PHandler(handler)
    , Counter(new std::atomic<std::uint64_t>{ 0 })
  {
  }

  void await(comm::Message&& msg)
  {
    std::uint64_t id = (*this->Counter)++;
    if (auto handler = this->PHandler.lock())
    {
      auto reply = handler->Preview(id, std::move(msg));
      if (reply.size() > 0)
      {
        this->LastMessagePair = std::make_pair(id, std::move(reply));
        this->State = stlab::yield_immediate;
      }
      else
      {
        // message has been processed in `preview` stage itself. Don't yield and
        // continue await more messages.
      }
    }
  }

  std::pair<std::uint64_t, comm::Message> yield()
  {
    auto msg_pair = std::move(this->LastMessagePair);
    this->State = stlab::await_forever;
    return msg_pair;
  }

  auto state() const { return this->State; }
};

class Session::SInternals
{
public:
  std::vector<stlab::receiver<void> > Holder;
  std::shared_ptr<ProxyHandler> PHandler;

  SInternals(Session* self) { this->PHandler = std::make_shared<ProxyHandler>(*self); }
};

//-----------------------------------------------------------------------------
Session::Session()
  : Internals(new Session::SInternals(this))
{
}

//-----------------------------------------------------------------------------
Session::~Session()
{
  this->Internals.reset();
  this->Service = nullptr;
}

//-----------------------------------------------------------------------------
bool Session::Initialize(const std::string& brokerURL, const std::string& name)
{
  this->Service = std::make_shared<comm::Service>(name);
  if (!stlab::blocking_get(this->Service->connect(brokerURL)))
  {
    this->Service = nullptr;
    return false;
  }

  // auto& service = this->Service;
  auto& internals = (*this->Internals);

  auto loop_executor = comm::loop_executor_type{};

  auto& recvr = this->Service->get_request_channel_receiver();
  auto phandler = internals.PHandler;
  auto hold = recvr | stlab::executor(stlab::default_executor) & PreviewProcess{ phandler } |
    stlab::buffer_size{ 100 } &
      [](std::pair<std::uint64_t, comm::Message>&& msg_pair) { return std::move(msg_pair); } |
    stlab::executor{ loop_executor } &
      [phandler](std::pair<std::uint64_t, comm::Message>&& msg_pair) {
        auto& id = msg_pair.first;
        auto& msg = msg_pair.second;
        phandler->Process(id, std::move(msg));
      };

  internals.Holder.push_back(std::move(hold));
  recvr.set_ready();
  return true;
}

//-----------------------------------------------------------------------------
const comm::Service& Session::GetService() const
{
  return *this->Service.get();
}

//-----------------------------------------------------------------------------
const ProxyHandler& Session::GetProxyHandler() const
{
  return *this->Internals->PHandler.get();
}
}
}
