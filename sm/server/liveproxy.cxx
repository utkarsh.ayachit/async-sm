#include "liveproxy.h"

#include <vtkDoubleArray.h>
#include <vtkFieldData.h>
#include <vtkImageData.h>
#include <vtkIntArray.h>
#include <vtkRTAnalyticSource.h>
#include <vtkSmartPointer.h>
#include <vtkTrivialProducer.h>

#include "session.h"

#include <comm/internal/serialization_pb.h>
#include <comm/message.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <comm/loop_executor.h>
#include <stlab/concurrency/default_executor.hpp>
#include <stlab/concurrency/utility.hpp>

#include <chrono>
#include <cmath>
#include <future>
#include <mutex>
#include <thread>

namespace sm
{
namespace server
{

namespace detail
{
static void addTime(vtkDataObject* dobj, int timestep, double time)
{
  vtkNew<vtkIntArray> ts;
  ts->SetName("timestep");
  ts->SetNumberOfTuples(1);
  ts->SetTypedComponent(0, 0, timestep);
  dobj->GetFieldData()->AddArray(ts);

  vtkNew<vtkDoubleArray> t;
  t->SetName("time");
  t->SetNumberOfTuples(1);
  t->SetTypedComponent(0, 0, time);
  dobj->GetFieldData()->AddArray(t);
}
}

class LiveProxy::LInternals
{
  const sm::server::Session& Session;
  std::mutex MutexInputs;
  int WholeExtent[6] = { 0, 1, 0, 1, 0, 1 };
  double Delay = 10.0;
  std::future<bool> ReturnCode;

  stlab::receiver<void> Receiver;

public:
  LInternals(const sm::server::Session& s)
    : Session(s)
  {
  }

  void SetWholeExtent(int xmin, int xmax, int ymin, int ymax, int zmin, int zmax)
  {
    std::lock_guard<std::mutex>(this->MutexInputs);
    this->WholeExtent[0] = xmin;
    this->WholeExtent[1] = xmax;
    this->WholeExtent[2] = ymin;
    this->WholeExtent[3] = ymax;
    this->WholeExtent[4] = zmin;
    this->WholeExtent[5] = zmax;
  }

  void SetDelay(double val)
  {
    std::lock_guard<std::mutex>(this->MutexInputs);
    this->Delay = val;
  }

  ~LInternals()
  {
    {
      std::lock_guard<std::mutex>(this->MutexInputs);
      this->Delay = -1;
    }
  }

  bool StartIfNeeded(vtkTrivialProducer* tp)
  {
    if (!this->ReturnCode.valid())
    {
      stlab::receiver<vtkSmartPointer<vtkImageData> > recvr;
      stlab::sender<vtkSmartPointer<vtkImageData> > sender;
      std::tie(sender, recvr) =
        stlab::channel<vtkSmartPointer<vtkImageData> >(stlab::immediate_executor);
      auto f = [this](stlab::sender<vtkSmartPointer<vtkImageData> >&& send) {
        std::uint64_t iteration = 0;
        int wextents[6];
        std::chrono::milliseconds delay;

        vtkNew<vtkRTAnalyticSource> source;
        do
        {
          {
            std::lock_guard<std::mutex> lk(this->MutexInputs);
            std::copy(this->WholeExtent, this->WholeExtent + 6, wextents);
            delay = std::chrono::milliseconds(static_cast<long>(this->Delay * 1000));
          }

          if (delay >= std::chrono::seconds{ 0 })
          {
            auto startT = std::chrono::steady_clock::now();
            source->SetWholeExtent(
              wextents[0], wextents[1], wextents[2], wextents[3], wextents[4], wextents[5]);
            source->SetXFreq(30 + (std::sin(iteration/5.0) * 30));
            source->Update();
            auto endT = std::chrono::steady_clock::now();
            auto delta = std::chrono::duration_cast<std::chrono::milliseconds>(endT - startT);
            if (delta < delay)
            {
              std::this_thread::sleep_for(std::chrono::milliseconds(delay - delta));
            }
            {
              auto img = vtkSmartPointer<vtkImageData>::New();
              img->ShallowCopy(source->GetOutput());
              detail::addTime(img, static_cast<int>(iteration), static_cast<double>(iteration));
              send(img);
            }
            // LOG_S(INFO) << "new live data available";
            this->Session.GetService().publish("liveupdate", comm::Message());
          }
          iteration++;
        } while (delay >= std::chrono::seconds{ 0 });
        return true;
      };
      this->ReturnCode = std::async(std::launch::async, f, std::move(sender));

      auto executor = comm::loop_executor_type{};
      this->Receiver = recvr |
        stlab::executor(executor) & [tp](vtkSmartPointer<vtkImageData> img) { tp->SetOutput(img); };
      recvr.set_ready();

      return true;
    }
    return false;
  }
};

//-----------------------------------------------------------------------------
LiveProxy::LiveProxy(const Session& session)
  : SourceProxy(vtkSmartPointer<vtkTrivialProducer>::New(), session)
  , Internals(new LiveProxy::LInternals(session))
{
  auto tp = vtkTrivialProducer::SafeDownCast(this->GetVTKObject());

  vtkNew<vtkRTAnalyticSource> tmp;
  tmp->Update();
  tp->SetOutput(tmp->GetOutput());
}

//-----------------------------------------------------------------------------
LiveProxy::~LiveProxy()
{
}

//-----------------------------------------------------------------------------
void LiveProxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->SourceProxy::UpdateState(pstate);
  if (auto tp = vtkTrivialProducer::SafeDownCast(this->GetVTKObject()))
  {
    for (const auto& pair : pstate.properties())
    {
      if (pair.first == "Delay" && pair.second.values().size() == 1 &&
        pair.second.values(0).value_case() == sm::common::Variant::kFloat64)
      {
        this->Internals->SetDelay(pair.second.values(0).float64());
      }
      else if (pair.first == "WholeExtent" && pair.second.values().size() == 6 &&
        pair.second.values(0).value_case() == sm::common::Variant::kInteger &&
        pair.second.values(1).value_case() == sm::common::Variant::kInteger &&
        pair.second.values(2).value_case() == sm::common::Variant::kInteger &&
        pair.second.values(3).value_case() == sm::common::Variant::kInteger &&
        pair.second.values(4).value_case() == sm::common::Variant::kInteger &&
        pair.second.values(5).value_case() == sm::common::Variant::kInteger)
      {
        this->Internals->SetWholeExtent(pair.second.values(0).integer(),
          pair.second.values(1).integer(), pair.second.values(2).integer(),
          pair.second.values(3).integer(), pair.second.values(4).integer(),
          pair.second.values(5).integer());
      }
    }
    this->Internals->StartIfNeeded(tp);
  }
}
}
}
