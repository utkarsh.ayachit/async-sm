#include "sliceproxy.h"

//#include <vtkImageResliceMapper.h>
//#include <vtkImageReslice.h>
#include <vtkPlaneCutter.h>
#include <vtkPlane.h>
#include <vtkSmartPointer.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

namespace sm
{
namespace server
{

//-----------------------------------------------------------------------------
SliceProxy::SliceProxy(const Session& session)
  : SourceProxy(vtkSmartPointer<vtkPlaneCutter>::New(), session)
{
//  auto slice = vtkPlaneCutter::SafeDownCast(this->GetVTKObject());
//  int extent[6];
//  slice->GetOutputExtent(extent);
//  extent[4] = extent[5] = 0;
//  slice->SetOutputExtent(extent);
//  slice->SetOutputDimensionality(2);
//  slice->SetResliceAxesDirectionCosines(1,1,0, -1,1,0, 0,0,1);
}

//-----------------------------------------------------------------------------
SliceProxy::~SliceProxy()
{
}

//-----------------------------------------------------------------------------
void SliceProxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->SourceProxy::UpdateState(pstate);
  if (auto slice = vtkPlaneCutter::SafeDownCast(this->GetVTKObject()))
  {
    for (const auto& pair : pstate.properties())
    {
      if (pair.first == "Origin")
      {
        auto& values = pair.second.values();
        assert(values.size() == 3);
        slice->GetPlane()->SetOrigin(values[0].float64(), values[1].float64(), values[2].float64());
      }
      else if (pair.first == "Normal")
      {
        auto& values = pair.second.values();
        assert(values.size() == 3);
        slice->GetPlane()->SetNormal(values[0].float64(), values[1].float64(), values[2].float64());
      }
    }
  }
}
}
}
