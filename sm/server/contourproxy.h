#ifndef sm_server_contourproxy_h
#define sm_server_contourproxy_h

#include "sourceproxy.h"

namespace sm
{
namespace server
{

class ContourProxy : public SourceProxy
{
public:
  ContourProxy(const Session& session);
  ~ContourProxy() override;

  void UpdateState(const sm::common::ProxyState& pstate) override;

private:
  ContourProxy(const ContourProxy&) = delete;
  void operator=(const ContourProxy&) = delete;
};
}
}

#endif
