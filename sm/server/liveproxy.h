#ifndef sm_server_liveproxy_h
#define sm_server_liveproxy_h

#include "waveletproxy.h"

#include <memory>

namespace sm
{
namespace server
{

/**
 * this dummy live source will just move the wavelet's center
 * at the rate specified
 */
class LiveProxy : public SourceProxy
{
public:
  LiveProxy(const Session& session);
  ~LiveProxy();

  void UpdateState(const sm::common::ProxyState& pstate) override;

private:
  LiveProxy(const LiveProxy&) = delete;
  void operator=(const LiveProxy&) = delete;

  class LInternals;
  std::unique_ptr<LInternals> Internals;
};
}
}

#endif
