#ifndef sm_server_senseiliveproxy_h
#define sm_server_senseiliveproxy_h

#include "waveletproxy.h"

#include <memory>

namespace sm
{
namespace server
{

/**
 * this live source acts as ADIOSAnalysisEndPoint embedded in ParaView.
 */
class SenseiLiveProxy : public SourceProxy
{
public:
  SenseiLiveProxy(const Session& session);
  ~SenseiLiveProxy();

  void UpdateState(const sm::common::ProxyState& pstate) override;

private:
  SenseiLiveProxy(const SenseiLiveProxy&) = delete;
  void operator=(const SenseiLiveProxy&) = delete;

  class LInternals;
  std::unique_ptr<LInternals> Internals;
};
}
}

#endif
