#include "vtkextensions.h"

#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkCellData.h>
#include <vtkCompositeDataIterator.h>
#include <vtkCompositeDataSet.h>
#include <vtkCompositePolyDataMapper2.h>
#include <vtkDataObject.h>
#include <vtkDataObjectAlgorithm.h>
#include <vtkDataSetWriter.h>
#include <vtkGeometryFilter.h>
#include <vtkInformation.h>
#include <vtkInformationObjectBaseKey.h>
#include <vtkInformationRequestKey.h>
#include <vtkIntArray.h>
#include <vtkLookupTable.h>
#include <vtkObject.h>
#include <vtkObjectFactory.h>
#include <vtkOutlineFilter.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkTextActor.h>
#include <vtkWindowToImageFilter.h>

#include <comm/loop_executor.h>
#include <sm/common/proxy.pb.h>

#include <comm/internal/serialization_pb.h>
#include <comm/message.h>

#include <chrono>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

namespace sm
{
namespace server
{

struct vtk_object_pointer_holder
{
  vtk_object_pointer_holder(vtkObject* obj)
    : Object(obj)
  {
    if (obj)
    {
      obj->Register(nullptr);
    }
  }
  ~vtk_object_pointer_holder()
  {
    if (this->Object)
    {
      this->Object->UnRegister(nullptr);
    }
  }
  vtkObject* Object;
};

namespace vtkextensions
{

void free_vtk_object_ptr(void* data, void* hint)
{
  (void)hint;
  delete reinterpret_cast<vtk_object_pointer_holder*>(data);
}

vtkSmartPointer<vtkObject> extract_vtk_object(const comm::Frame& msg)
{
  auto pholder = reinterpret_cast<const vtk_object_pointer_holder*>(msg.data());
  return pholder->Object;
}

comm::Frame pack_vtk_object(vtkSmartPointer<vtkObject> obj)
{
  auto hldr = new vtk_object_pointer_holder(obj);
  return comm::Frame(hldr, sizeof(vtk_object_pointer_holder), free_vtk_object_ptr, nullptr);
}

void free_by_delete_function(void* data, void* hint)
{
  (void)hint;
  delete[] reinterpret_cast<char*>(data);
}

namespace detail
{
bool FindScalars(vtkDataObject* dObj, std::string& arrayName, int& scalarMode)
{
  if (auto cd = vtkCompositeDataSet::SafeDownCast(dObj))
  {
    auto iter = cd->NewIterator();
    for (iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
    {
      if (detail::FindScalars(iter->GetCurrentDataObject(), arrayName, scalarMode))
      {
        return true;
      }
    }
    iter->Delete();
  }
  else if (auto ds = vtkDataSet::SafeDownCast(dObj))
  {
    if (auto pscalars = ds->GetPointData()->GetScalars())
    {
      arrayName = pscalars->GetName();
      scalarMode = VTK_SCALAR_MODE_USE_POINT_FIELD_DATA;
      return true;
    }
    else if (auto cscalars = ds->GetCellData()->GetScalars())
    {
      arrayName = cscalars->GetName();
      scalarMode = VTK_SCALAR_MODE_USE_CELL_FIELD_DATA;
      return true;
    }
    for (int cc = 0; cc < ds->GetPointData()->GetNumberOfArrays(); ++cc)
    {
      if (auto array = ds->GetPointData()->GetArray(cc))
      {
        arrayName = array->GetName();
        scalarMode = VTK_SCALAR_MODE_USE_POINT_FIELD_DATA;
        return true;
      }
    }
    for (int cc = 0; cc < ds->GetCellData()->GetNumberOfArrays(); ++cc)
    {
      if (auto array = ds->GetCellData()->GetArray(cc))
      {
        arrayName = array->GetName();
        scalarMode = VTK_SCALAR_MODE_USE_CELL_FIELD_DATA;
        return true;
      }
    }
  }
  return false;
}

vtkVector2d ComputeRange(vtkDataObject* dObj, const std::string& arrayName, int smode)
{
  if (auto cd = vtkCompositeDataSet::SafeDownCast(dObj))
  {
    vtkVector2d range{ VTK_DOUBLE_MAX, VTK_DOUBLE_MIN };
    auto iter = cd->NewIterator();
    for (iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
    {
      auto r = detail::ComputeRange(iter->GetCurrentDataObject(), arrayName, smode);
      if (r[0] <= r[1])
      {
        range[0] = std::min(range[0], r[0]);
        range[1] = std::max(range[1], r[1]);
      }
    }
    iter->Delete();
    return range;
  }
  else if (auto ds = vtkDataSet::SafeDownCast(dObj))
  {
    switch (smode)
    {
      case VTK_SCALAR_MODE_USE_POINT_FIELD_DATA:
        if (auto scalars = ds->GetPointData()->GetArray(arrayName.c_str()))
        {
          vtkVector2d r;
          scalars->GetRange(r.GetData(), -1);
          return r;
        }
        break;
      case VTK_SCALAR_MODE_USE_CELL_FIELD_DATA:
        if (auto scalars = ds->GetPointData()->GetArray(arrayName.c_str()))
        {
          vtkVector2d r;
          scalars->GetRange(r.GetData(), -1);
          return r;
        }
        break;
    }
  }
  return vtkVector2d(0, -1);
}

vtkSmartPointer<vtkLookupTable> GetLookupTable(const std::string& arrayName, vtkVector2d& range)
{
  // silly global map to "manage" luts for now.
  static std::map<std::string, vtkSmartPointer<vtkLookupTable> > globalMap;
  auto iter = globalMap.find(arrayName);
  if (iter == globalMap.end())
  {
    auto lut = vtkSmartPointer<vtkLookupTable>::New();
    lut->SetVectorModeToMagnitude();
    lut->SetTableRange(range.GetData());
    globalMap[arrayName] = lut;
    return lut;
  }
  else
  {
    auto lut = iter->second;
    vtkVector2d crange;
    lut->GetTableRange(crange.GetData());
    crange[0] = std::min(crange[0], range[0]);
    crange[1] = std::max(crange[1], range[1]);
    lut->SetTableRange(crange.GetData());
    return lut;
  }
}

void UpdateScalarColoring(vtkDataObject* data, vtkCompositePolyDataMapper2* mapper)
{
  std::string arrayName;
  int scalarMode;
  if (detail::FindScalars(data, arrayName, scalarMode))
  {
    mapper->ScalarVisibilityOn();
    // use LUT.
    mapper->UseLookupTableScalarRangeOn();
    mapper->SetColorModeToMapScalars();
    mapper->SetScalarMode(scalarMode);
    mapper->SelectColorArray(arrayName.c_str());

    auto range = detail::ComputeRange(data, arrayName, scalarMode);
    auto lut = detail::GetLookupTable(arrayName, range);
    mapper->SetLookupTable(lut);
  }
  else
  {
    mapper->ScalarVisibilityOff();
  }
}

void UpdateTimeAnnotation(vtkDataObject* data, vtkTextActor* textActor)
{
  if (auto timestep = vtkIntArray::SafeDownCast(data->GetFieldData()->GetArray("timestep")))
  {
    textActor->SetInput(
      (std::string("timestep: ") + std::to_string(timestep->GetTypedComponent(0, 0))).c_str());
  }
  else
  {
    textActor->SetInput("timestep: n/a");
  }
}
}

//==============================================================================
vtkStandardNewMacro(vtkPVRenderView);
vtkInformationKeyMacro(vtkPVRenderView, REQUEST_DATA, Request);
vtkInformationKeyMacro(vtkPVRenderView, REQUEST_RENDER, Request);
vtkInformationKeyMacro(vtkPVRenderView, VIEW, ObjectBase);
//-----------------------------------------------------------------------------
vtkPVRenderView::vtkPVRenderView()
{
  this->Renderer->SetBackground2(0.7, 0.7, 0.7);
  // this->Renderer->SetBackground2(0, 1, 1);
  this->Renderer->GradientBackgroundOn();
  this->Window->OffScreenRenderingOn();
  this->Window->AddRenderer(this->Renderer);

  this->W2IFilter->SetInput(this->Window);
  this->W2IFilter->ReadFrontBufferOff();
  this->W2IFilter->ShouldRerenderOn();

  this->SetSize(400, 400);

  this->Writer->WriteToOutputStringOn();
  this->Writer->SetFileTypeToBinary();
}

//-----------------------------------------------------------------------------
void vtkPVRenderView::Update(const sm::common::UpdatePipeline& vtkNotUsed(req),
  const sm::server::Session& session,
  const std::string& vtkNotUsed(channel))
{
  this->CurrentSession = &session;

  vtkNew<vtkInformation> request;
  request->Set(vtkPVRenderView::REQUEST_DATA());
  request->Set(vtkPVRenderView::VIEW(), this);

  vtkTimeStamp ts;
  ts.Modified();
  for (auto& repr : this->Representations)
  {
    repr->Update();
    if (repr->GetPipelineDataTime() > ts)
    {
      repr->ProcessRequest(request, nullptr, nullptr);
    }
  }

  this->CurrentSession = nullptr;
}

//-----------------------------------------------------------------------------
void vtkPVRenderView::PublishDataForRendering(
  vtkInformation* request, vtkGeometryRepresentation* repr, vtkSmartPointer<vtkDataObject> dataObj)
{
  LOG_S(6) << "PublishDataForRendering";
  auto view = vtkPVRenderView::SafeDownCast(request->Get(vtkPVRenderView::VIEW()));

  comm::Message msg;
  msg.push_back(pack_vtk_object(dataObj));
  uint64_t rid = repr->GetGlobalID();
  msg.push_back(comm::Frame(&rid, sizeof(rid)));
  view->CurrentSession->GetService().publish(
    "renderingdata." + std::to_string(view->GetGlobalID()), std::move(msg));
}

//-----------------------------------------------------------------------------
void vtkPVRenderView::ProcessRenderingData(vtkGeometryRepresentation* repr,
  vtkSmartPointer<vtkDataObject> data, const sm::server::Session& session)
{
  vtkNew<vtkInformation> request;
  request->Set(vtkPVRenderView::REQUEST_RENDER());
  request->Set(vtkPVRenderView::VIEW(), this);
  // hack need to support multiple inputs etc.
  request->Set(vtkDataObject::DATA_OBJECT(), data);
  repr->ProcessRequest(request, nullptr, nullptr);

  this->Render(session);
}

//-----------------------------------------------------------------------------
vtkVector<double, 6> vtkPVRenderView::GetPropBounds()
{
  vtkVector<double, 6> bds;
  this->Renderer->ComputeVisiblePropBounds(bds.GetData());
  return bds;
}

//-----------------------------------------------------------------------------
void vtkPVRenderView::Render(const sm::common::Render& req,
  const sm::server::Session& session,
  const std::string& vtkNotUsed(channel))
{
  if (auto camera = this->Renderer->GetActiveCamera())
  {
    camera->SetPosition(req.position().x(), req.position().y(), req.position().z());
    camera->SetFocalPoint(req.focal_point().x(), req.focal_point().y(), req.focal_point().z());
    camera->SetViewUp(req.view_up().x(), req.view_up().y(), req.view_up().z());
  }
  this->Window->SetSize(req.size().x(), req.size().y());
  this->Render(session);
}

//-----------------------------------------------------------------------------
void vtkPVRenderView::Render(const sm::server::Session& session)
{
  LOG_S(6) << "render start";
  this->Renderer->ResetCameraClippingRange();
  this->W2IFilter->Modified();
  this->W2IFilter->Update();

  auto img = vtkSmartPointer<vtkImageData>::New();
  img->ShallowCopy(this->W2IFilter->GetOutput());

  comm::Message msg;
  msg.push_back(pack_vtk_object(img));
  session.GetService().publish("render." + std::to_string(this->GetGlobalID()), std::move(msg));
  LOG_S(6) << "render end";

  //// todo: do the writing on a separate thread.
  // this->Writer->SetInputDataObject(this->W2IFilter->GetOutput());
  // this->Writer->Modified();
  // this->Writer->Update();

  // vtkIdType size = this->Writer->GetOutputStringLength();
  // char* data = this->Writer->RegisterAndGetOutputString();

  // comm::Message msg;
  // msg.push_back(comm::Frame(data, size, free_by_delete_function, nullptr));
  // session.GetService().Publish("render." + std::to_string(this->GetGlobalID()),
  // std::move(msg));

#if 0
  this->Writer->WriteToOutputStringOff();
  this->Writer->SetFileName("/tmp/foo.vtk");
  this->Writer->Update();
  this->Writer->WriteToOutputStringOn();
#endif
}

//-----------------------------------------------------------------------------
void vtkPVRenderView::SetSize(int dx, int dy)
{
  this->Window->SetSize(dx, dy);
  this->Modified();
}

//-----------------------------------------------------------------------------
void vtkPVRenderView::SetRepresentations(
  const std::vector<vtkSmartPointer<vtkGeometryRepresentation> >& reprs)
{
  if (this->Representations != reprs)
  {
    LOG_S(INFO) << "settings representations (size=" << reprs.size() << ")";
    this->Representations = reprs;
    // hack
    for (auto& repr : this->Representations)
    {
      repr->AddToView(this);
    }
    this->Modified();
  }
}

vtkRenderer* vtkPVRenderView::GetRenderer()
{
  return this->Renderer;
}

//==============================================================================
vtkStandardNewMacro(vtkGeometryRepresentation);
//-----------------------------------------------------------------------------
vtkGeometryRepresentation::vtkGeometryRepresentation()
{
  this->SetNumberOfInputPorts(1);
  this->SetNumberOfOutputPorts(0);

  this->Actor->SetMapper(this->Mapper);
  vtkNew<vtkPolyData> temp;
  this->Mapper->SetInputDataObject(temp);

  this->TextActor->SetVisibility(false);
  this->TextActor->SetInput("Time: ");
}

//-----------------------------------------------------------------------------
void vtkGeometryRepresentation::AddToView(vtkPVRenderView* view)
{
  if (this->View == nullptr)
  {
    this->View = view;
    view->GetRenderer()->AddActor(this->Actor);
    view->GetRenderer()->AddActor(this->TextActor);
  }
}

//-----------------------------------------------------------------------------
void vtkGeometryRepresentation::ShowTimeAnnotation(bool val)
{
  this->TextActor->SetVisibility(val);
}

//-----------------------------------------------------------------------------
int vtkGeometryRepresentation::ProcessRequest(
  vtkInformation* request, vtkInformationVector** inInfo, vtkInformationVector* outInfo)
{
  if (request->Has(vtkPVRenderView::REQUEST_DATA()))
  {
    // provide geometry for rendering, let's force non-null, can be empty, but
    // must not be null.
    assert(this->GeometryToDeliver != nullptr);
    vtkPVRenderView::PublishDataForRendering(request, this, this->GeometryToDeliver);
    return 1;
  }
  else if (request->Has(vtkPVRenderView::REQUEST_RENDER()))
  {
    auto data = request->Get(vtkDataObject::DATA_OBJECT());
    assert(data);
    this->Mapper->SetInputDataObject(data);
    detail::UpdateScalarColoring(data, this->Mapper);
    detail::UpdateTimeAnnotation(data, this->TextActor);
    return 1;
  }
  else
  {
    return this->Superclass::ProcessRequest(request, inInfo, outInfo);
  }
}

//-----------------------------------------------------------------------------
int vtkGeometryRepresentation::FillInputPortInformation(int port, vtkInformation* info)
{
  this->Superclass::FillInputPortInformation(port, info);
  info->Set(vtkAlgorithm::INPUT_IS_OPTIONAL(), 1);
  return 1;
}

//-----------------------------------------------------------------------------
int vtkGeometryRepresentation::RequestData(
  vtkInformation*, vtkInformationVector** inputVector, vtkInformationVector*)
{
  this->PipelineDataTime.Modified();

  this->GeometryToDeliver = nullptr;
  vtkDataObject* input = vtkDataObject::GetData(inputVector[0], 0);
  if (input == nullptr)
  {
    return 1;
  }

  bool is_structured = (input->GetExtentType() == VTK_3D_EXTENT);
  auto mb = vtkCompositeDataSet::SafeDownCast(input);
  if (!is_structured && mb)
  {
    auto iter = mb->NewIterator();
    for (iter->InitTraversal(); !iter->IsDoneWithTraversal(); iter->GoToNextItem())
    {
      is_structured = (iter->GetCurrentDataObject()->GetExtentType() == VTK_3D_EXTENT);
      if (is_structured)
      {
        break;
      }
    }
    iter->Delete();
  }

  vtkSmartPointer<vtkDataObject> geom;
  if (is_structured)
  {
    this->OutlineFilter->SetInputDataObject(input);
    this->OutlineFilter->Update();
    this->OutlineFilter->SetInputDataObject(nullptr);
    geom = this->OutlineFilter->GetOutputDataObject(0);
  }
  else
  {
    this->GeometryFilter->SetInputDataObject(input);
    this->GeometryFilter->Update();
    this->GeometryFilter->SetInputDataObject(nullptr);
    geom = this->GeometryFilter->GetOutputDataObject(0);
  }

  if (geom)
  {
    this->GeometryToDeliver.TakeReference(geom->NewInstance());
    this->GeometryToDeliver->ShallowCopy(geom);
    geom->Initialize();
  }
  return 1;
}

//==============================================================================
}
}
}
