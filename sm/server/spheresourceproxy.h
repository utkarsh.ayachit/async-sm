#ifndef sm_server_spheresourceproxy_h
#define sm_server_spheresourceproxy_h

#include "sourceproxy.h"

namespace sm
{
namespace server
{
class SphereSourceProxy : public SourceProxy
{
public:
  SphereSourceProxy(const Session& session);
  ~SphereSourceProxy();

  void UpdateState(const sm::common::ProxyState& pstate) override;

private:
  SphereSourceProxy(const SphereSourceProxy&) = delete;
  void operator=(const SphereSourceProxy&) = delete;
};
}
}

#endif
