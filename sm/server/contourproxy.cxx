#include "contourproxy.h"

#include <vtkContourFilter.h>
#include <vtkSmartPointer.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

namespace sm
{
namespace server
{

//-----------------------------------------------------------------------------
ContourProxy::ContourProxy(const Session& session)
  : SourceProxy(vtkSmartPointer<vtkContourFilter>::New(), session)
{
}

//-----------------------------------------------------------------------------
ContourProxy::~ContourProxy()
{
}

//-----------------------------------------------------------------------------
void ContourProxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->SourceProxy::UpdateState(pstate);
  if (auto contour = vtkContourFilter::SafeDownCast(this->GetVTKObject()))
  {
    for (const auto& pair : pstate.properties())
    {
      if (pair.first == "IsoValues")
      {
        contour->SetNumberOfContours(pair.second.values().size());
        int index = 0;
        for (const auto& value : pair.second.values())
        {
          contour->SetValue(index++, value.float64());
        }
      }
    }
  }
}
}
}
