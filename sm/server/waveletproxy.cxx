#include "waveletproxy.h"

#include <vtkRTAnalyticSource.h>
#include <vtkSmartPointer.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

namespace sm
{
namespace server
{

//-----------------------------------------------------------------------------
WaveletProxy::WaveletProxy(const Session& session)
  : SourceProxy(vtkSmartPointer<vtkRTAnalyticSource>::New(), session)
{
}

//-----------------------------------------------------------------------------
WaveletProxy::~WaveletProxy()
{
}

//-----------------------------------------------------------------------------
void WaveletProxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->SourceProxy::UpdateState(pstate);
  if (auto wavelet = vtkRTAnalyticSource::SafeDownCast(this->GetVTKObject()))
  {
    for (const auto& pair : pstate.properties())
    {
      if (pair.first == "Center" && pair.second.values().size() == 3 &&
        pair.second.values(0).value_case() == sm::common::Variant::kFloat64 &&
        pair.second.values(1).value_case() == sm::common::Variant::kFloat64 &&
        pair.second.values(2).value_case() == sm::common::Variant::kFloat64)
      {
        wavelet->SetCenter(pair.second.values(0).float64(), pair.second.values(1).float64(),
          pair.second.values(2).float64());
      }
      else if (pair.first == "WholeExtent" && pair.second.values().size() == 6 &&
        pair.second.values(0).value_case() == sm::common::Variant::kInteger &&
        pair.second.values(1).value_case() == sm::common::Variant::kInteger &&
        pair.second.values(2).value_case() == sm::common::Variant::kInteger &&
        pair.second.values(3).value_case() == sm::common::Variant::kInteger &&
        pair.second.values(4).value_case() == sm::common::Variant::kInteger &&
        pair.second.values(5).value_case() == sm::common::Variant::kInteger)
      {
        wavelet->SetWholeExtent(pair.second.values(0).integer(), pair.second.values(1).integer(),
          pair.second.values(2).integer(), pair.second.values(3).integer(),
          pair.second.values(4).integer(), pair.second.values(5).integer());
      }
    }
  }
}
}
}
