#include "renderviewproxy.h"

#include <comm/internal/serialization_pb.h>

#include "vtkextensions.h"
#include <vtkDataSetReader.h>
#include <vtkSmartPointer.h>

#include <comm/internal/serialization_pb.h>
#include <comm/message.h>

#include <comm/loop_executor.h>
#include <comm/service.h>
#include <sm/server/proxyhandler.h>
#include <sm/server/session.h>
#include <zmq.hpp>

#include <sstream>
#include <utility>

#define LOGURU_WITH_STREAMS 1
#include <stlab/concurrency/default_executor.hpp>
#include <thirdparty/loguru/loguru.hpp>

namespace sm
{
namespace server
{

class RenderViewProxy::RVPInternals
{
public:
  std::atomic<std::uint64_t> UpdatePipelineToken{ 0 };
  std::atomic<std::uint64_t> RenderToken{ 0 };
};

//-----------------------------------------------------------------------------
RenderViewProxy::RenderViewProxy(const Session& session)
  : Proxy(vtkSmartPointer<vtkextensions::vtkPVRenderView>::New(), session)
  , Internals(new RenderViewProxy::RVPInternals())
{
}

//-----------------------------------------------------------------------------
RenderViewProxy::~RenderViewProxy()
{
}

//-----------------------------------------------------------------------------
void RenderViewProxy::SetGlobalID(uint64_t id)
{
  this->Superclass::SetGlobalID(id);
  auto view = vtkextensions::vtkPVRenderView::SafeDownCast(this->GetVTKObject());
  view->SetGlobalID(id);

  std::ostringstream channel;
  channel << "render." << id;
  this->Channel = channel.str();

  const auto& service = this->GetSession().GetService();
  if (service.get_name() == "rs") // subscribe only on rendering ranks.
  {
    // subscribe to channel to get new rendering geometries.
    auto executor = comm::loop_executor_type{};
    auto recvr = this->GetSession().GetService().subscribe("renderingdata." + std::to_string(id));
    this->GeometryReceiver = recvr |
      [](std::shared_ptr<const comm::Message> msg) {
        LOG_S(1) << "got raw data " << msg->get_debug_string();
        assert(msg->size() == 2);
        auto obj = vtkextensions::extract_vtk_object(msg->at(0));
        vtkSmartPointer<vtkDataObject> dobj = vtkDataObject::SafeDownCast(obj);
        // FIXME: use protobuf
        uint64_t rid = *reinterpret_cast<const uint64_t*>(msg->at(1).data());
        return std::make_pair(rid, dobj);
      } |
      stlab::executor(executor) &
        [view, &session = this->GetSession()](
          const std::pair<uint64_t, vtkSmartPointer<vtkDataObject> >&& data_pair) {
          // do on main thread.
          LOG_S(1) << "got decoded data";
          auto rproxy = session.GetProxyHandler().LocateProxy(data_pair.first);
          if (rproxy)
          {
            if (auto r =
                  vtkextensions::vtkGeometryRepresentation::SafeDownCast(rproxy->GetVTKObject()))
            {
              view->ProcessRenderingData(r, data_pair.second, session);
            }
          }
        };
    recvr.set_ready();

#if 0
    auto recvr = this->GetSession().GetService().Subscribe("renderingdata." + std::to_string(id));
    this->GeometryReceiver = recvr |
      [](const comm::Message& msg) {
        // do on subsciber thread.
        LOG_S(1) << "got raw data";
        assert(msg.size() == 2);
        auto reader = vtkSmartPointer<vtkDataSetReader>::New();
        reader->ReadFromInputStringOn();
        reader->SetBinaryInputString(
          reinterpret_cast<const char*>(msg.at(0)->data()), msg.at(0)->size());
        // FIXME: use protobuf
        uint64_t rid = *reinterpret_cast<uint64_t*>(msg.at(1)->data());
        return std::make_pair(rid, reader);
      } |
      stlab::executor(stlab::default_executor) &
        [](std::pair<uint64_t, vtkSmartPointer<vtkDataSetReader> > dpair) {
          // do on random thread.
          LOG_S(1) << "got raw data -- reading";
          dpair.second->Update();
          return std::make_pair(
            dpair.first, vtkSmartPointer<vtkDataObject>(dpair.second->GetOutputDataObject(0)));
        } |
      stlab::executor(executor) &
        [ view, &session = this->GetSession() ](
          const std::pair<uint64_t, vtkSmartPointer<vtkDataObject> > data_pair)
    {
      // do on main thread.
      LOG_S(1) << "got decoded data";
      auto rproxy = session.GetProxyHandler().LocateProxy(data_pair.first);
      if (rproxy)
      {
        if (auto r = vtkextensions::vtkGeometryRepresentation::SafeDownCast(rproxy->GetVTKObject()))
        {
          view->ProcessRenderingData(r, data_pair.second, session);
        }
      }
    };
    recvr.set_ready();
#endif
  }
}

//-----------------------------------------------------------------------------
void RenderViewProxy::UpdateState(const sm::common::ProxyState& pstate)
{
  this->Superclass::UpdateState(pstate);

  if (auto view = vtkextensions::vtkPVRenderView::SafeDownCast(this->GetVTKObject()))
  {
    for (const auto& pair : pstate.properties())
    {
      if (pair.first == "Representations")
      {
        std::vector<vtkSmartPointer<vtkextensions::vtkGeometryRepresentation> > reprs;
        for (const auto& value : pair.second.values())
        {
          if (value.value_case() == sm::common::Variant::kProxy)
          {
            auto rproxy = this->LocateProxy(value.proxy());
            if (auto r =
                  vtkextensions::vtkGeometryRepresentation::SafeDownCast(rproxy->GetVTKObject()))
            {
              reprs.push_back(r);
            }
          }
        }
        view->SetRepresentations(reprs);
      }
    }
  }
}

//-----------------------------------------------------------------------------
bool RenderViewProxy::Preview(std::uint64_t msg_num,
  const comm::Message& msg,
  const sm::common::ProxyGenericRequest& req)
{
  if (req.body().Is<sm::common::UpdatePipeline>())
  {
    this->Internals->UpdatePipelineToken = msg_num;
  }
  else if (req.body().Is<sm::common::Render>())
  {
    this->Internals->RenderToken = msg_num;
  }
  return this->Superclass::Preview(msg_num, msg, req);
}

//-----------------------------------------------------------------------------
bool RenderViewProxy::Process(std::uint64_t msg_num,
  const comm::Message& msg,
  const sm::common::ProxyGenericRequest& req)
{
  if (this->Superclass::Process(msg_num, msg, req))
  {
    return true;
  }

  auto& service = this->GetSession().GetService();
  if (req.body().Is<sm::common::UpdatePipeline>())
  {
    LOG_S(INFO) << "RenderViewProxy::Process::UpdatePipeline";
    if (auto view = vtkextensions::vtkPVRenderView::SafeDownCast(this->GetVTKObject()))
    {
      if (this->Internals->UpdatePipelineToken == msg_num)
      {
        sm::common::UpdatePipeline up;
        req.body().UnpackTo(&up);
        LOG_S(INFO) << "do view update";
        view->Update(up, this->GetSession(), this->Channel);
      }
      else
      {
        LOG_S(INFO) << "ignoring update";
      }
    }
    service.send_reply(msg, comm::Message());
    return true;
  }
  else if (req.body().Is<sm::common::Render>())
  {
    LOG_S(6) << "RenderViewProxy::Process::Render";
    if (this->Internals->RenderToken == msg_num)
    {
      sm::common::Render render;
      req.body().UnpackTo(&render);
      this->Render(render);
    }
    else
    {
      LOG_S(INFO) << "ignoring render";
    }
    service.send_reply(msg, comm::Message());
    return true;
  }
  else if (req.body().Is<sm::common::Variant>())
  {
    sm::common::Variant var;
    req.body().UnpackTo(&var);
    if (var.value_case() == sm::common::Variant::kStr)
    {
      if (var.str() == "GetPropBounds")
      {
        if (auto view = vtkextensions::vtkPVRenderView::SafeDownCast(this->GetVTKObject()))
        {
          auto pbds = view->GetPropBounds();
          sm::common::Bounds bds;
          bds.set_xmin(pbds[0]);
          bds.set_xmax(pbds[1]);
          bds.set_ymin(pbds[2]);
          bds.set_ymax(pbds[3]);
          bds.set_zmin(pbds[4]);
          bds.set_zmax(pbds[5]);

          sm::common::ProxyGenericResponse response;
          response.mutable_body()->PackFrom(bds);

          comm::Message reply;
          reply.push_back(response);
          service.send_reply(msg, std::move(reply));
          return true;
        }
      }
    }
    LOG_S(ERROR) << "Unhandled command: " << req.ShortDebugString();
    service.send_reply(msg, comm::Message());
    return true;
  }

  return false;
}

//-----------------------------------------------------------------------------
void RenderViewProxy::Render(const sm::common::Render& renderReq)
{
  if (auto view = vtkextensions::vtkPVRenderView::SafeDownCast(this->GetVTKObject()))
  {
    view->Render(renderReq, this->GetSession(), this->Channel);
  }
}
}
}
