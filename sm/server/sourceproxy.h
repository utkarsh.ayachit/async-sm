#ifndef sm_server_sourceproxy_h
#define sm_server_sourceproxy_h

#include "proxy.h"

class vtkAlgorithm;
namespace sm
{
namespace server
{
class SourceProxy : public Proxy
{
  using Superclass = Proxy;

public:
  SourceProxy(vtkAlgorithm* obj, const Session& session);
  ~SourceProxy();

  vtkAlgorithm* GetVTKAlgorithm() const;

  bool Preview(std::uint64_t msg_num,
    const comm::Message& msg,
    const sm::common::ProxyGenericRequest& req) override;
  bool Process(std::uint64_t msg_num,
    const comm::Message& msg,
    const sm::common::ProxyGenericRequest& req) override;

protected:
  void UpdateState(const sm::common::ProxyState& pstate) override;

private:
  void UpdatePipeline(double time);

private:
  SourceProxy(const SourceProxy&) = delete;
  void operator=(const SourceProxy&) = delete;

  std::atomic<std::uint64_t> MakeDirtyMsgTag;
};
}
}

#endif
