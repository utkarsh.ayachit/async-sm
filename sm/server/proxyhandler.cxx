#include "proxyhandler.h"

#include "proxy.h"
#include "session.h"

#include <comm/internal/serialization_pb.h>
#include <comm/message.h>

#include <comm/loop_executor.h>
#include <comm/message.h>
#include <comm/service.h>
#include <comm/zmq.h>
#include <sm/common/proxy.pb.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/utility.hpp>

#include <chrono>

namespace sm
{
namespace server
{
class ProxyHandler::PHInternals
{
public:
  const Session& ParentSession;
  std::map<std::uint64_t, std::shared_ptr<Proxy> > Proxies;

  PHInternals(const Session& ref)
    : ParentSession(ref)
  {
  }

  bool Preview(std::uint64_t msg_num,
    const comm::Message& msg,
    const sm::common::ProxyGenericRequest& req)
  {
    const auto proxyid = req.global_id();
    std::shared_ptr<Proxy> proxy;
    try
    {
      proxy = this->Proxies.at(proxyid);
      CHECK_F(proxy != nullptr, "Failed to locate proxy: %s", req.ShortDebugString().c_str());
      return proxy->Preview(msg_num, msg, req);
    }
    catch (std::out_of_range)
    {
      return false;
    }
  }

  bool Process(std::uint64_t msg_num,
    const comm::Message& msg,
    const sm::common::ProxyGenericRequest& req)
  {
    const auto proxyid = req.global_id();
    std::shared_ptr<Proxy> proxy;
    try
    {
      proxy = this->Proxies.at(proxyid);
    }
    catch (std::out_of_range)
    {
      // create a new proxy if this is state message.
      if (req.body().Is<sm::common::ProxyState>())
      {
        sm::common::ProxyState pstate;
        req.body().UnpackTo(&pstate);
        proxy = Proxy::Create(pstate.xml_group(), pstate.xml_name(), this->ParentSession);
        assert(proxy);
        this->Proxies[proxyid] = proxy;
        proxy->SetGlobalID(proxyid);
      }
      else
      {
        return false;
      }
    }
    CHECK_F(
      proxy != nullptr, "Failed to locate (or create) proxy: %s", req.ShortDebugString().c_str());
    proxy->Process(msg_num, msg, req);
    return true;
  }
};

//-----------------------------------------------------------------------------
ProxyHandler::ProxyHandler(const Session& session)
  : Internals(new ProxyHandler::PHInternals(session))
{
}

//-----------------------------------------------------------------------------
ProxyHandler::~ProxyHandler()
{
}

//-----------------------------------------------------------------------------
comm::Message ProxyHandler::Preview(std::uint64_t msg_num, comm::Message&& msg)
{
  auto& internals = (*this->Internals);
  sm::common::ProxyGenericRequest pupr;
  if (msg.size() > 0)
  {
    if (comm::z2p(msg.back(), &pupr))
    {
      if (internals.Preview(msg_num, msg, pupr))
      {
        // empty message will cause the message to no be forwarded to "Process"
        // stage.
        return comm::Message();
      }
      else
      {
        return std::move(msg);
      }
    }
  }
  LOG_S(ERROR) << "unhandled message (preview): " << msg.get_debug_string();
  return std::move(msg);
}

//-----------------------------------------------------------------------------
void ProxyHandler::Process(std::uint64_t msg_num, comm::Message&& msg)
{
  auto& internals = (*this->Internals);
  sm::common::ProxyGenericRequest pupr;
  if (msg.size() > 0)
  {
    if (comm::z2p(msg.back(), &pupr))
    {
      if (internals.Process(msg_num, msg, pupr))
      {
        // all's well that ends well.
        return;
      }
    }
  }
  LOG_S(ERROR) << "unhandled message (process): " << msg.get_debug_string();
}

//-----------------------------------------------------------------------------
const Session& ProxyHandler::GetSession() const
{
  return this->Internals->ParentSession;
}

//-----------------------------------------------------------------------------
std::shared_ptr<Proxy> ProxyHandler::LocateProxy(uint64_t gid) const
{
  try
  {
    return this->Internals->Proxies.at(gid);
  }
  catch (std::out_of_range)
  {
    return nullptr;
  }
}

//-----------------------------------------------------------------------------
}
}
