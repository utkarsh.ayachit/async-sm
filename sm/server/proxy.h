#ifndef sm_server_proxy_h
#define sm_server_proxy_h

#include <memory>
#include <sm/common/proxy.pb.h>

namespace comm
{
class Message;
}

class vtkObject;
namespace sm
{
namespace server
{

class Session;

/**
 * @class Proxy
 * @brief mockup for SIProxy
 *
 * Note, Proxy and all its subclasses are just here in place of their ParaView
 * counterparts for the demo. Ultimately, these should be replaced by the
 * ParaView classes and hence the API on these classes has no bearing.
 */

class Proxy
{
public:
  Proxy(vtkObject* obj, const Session& session);
  virtual ~Proxy();

  virtual void SetGlobalID(uint64_t id) { this->GlobalID = id; }
  uint64_t GetGlobalID() const { return this->GlobalID; }

  //@{
  /**
   * APIs for preview/process.
   */
  virtual bool Preview(std::uint64_t msg_num,
    const comm::Message& msg,
    const sm::common::ProxyGenericRequest& req);
  virtual bool Process(std::uint64_t msg_num,
    const comm::Message& msg,
    const sm::common::ProxyGenericRequest& req);
  //@}

  static std::shared_ptr<Proxy> Create(
    const std::string& xmlgroup, const std::string& xmlname, const Session& session);

  vtkObject* GetVTKObject() const;
  const Session& GetSession() const;

protected:
  std::shared_ptr<Proxy> LocateProxy(uint64_t gid);

  /**
   */
  virtual void UpdateState(const sm::common::ProxyState& pstate);

private:
  Proxy(const Proxy&) = delete;
  void operator=(const Proxy&) = delete;

  class PInternals;
  std::unique_ptr<PInternals> Internals;

  uint64_t GlobalID = 0;
};
}
}

#endif
