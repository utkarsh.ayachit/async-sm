Build instructions
===================

* Note this repo has submodules; ensure that `git submodule update --init` is called after cloning.
* Requires clang or VS2017

External dependencies
----------------------
* [zeromq v4.2.3](https://github.com/zeromq/libzmq/releases/download/v4.2.3/zeromq-4.2.3.zip)
* [protobuf v3.6](https://github.com/google/protobuf/releases/tag/v3.6.0)
* [cppzmq](https://github.com/zeromq/cppzmq/blob/master/zmq.hpp): just plop the zmq.hpp header file in the include path somewhere.
* VTK: latest master with Qt support.
* Qt 5.9+
* Boost 1.65+

Icons
-----
* Icons are from ParaView and [Material icons](https://material.io/tools/icons/?icon=info&style=baseline).

Building in Windows
--------------------
Following steps have been tested for building in windows.
1. Install Microsoft Visual Studio 2017.
2. Install CMake 3.11.1 (older versions might work too).
3. Install Boost 1.65+
4. Install Qt 5.9+ for msvc2017 64-bit
5. Build and Install VTK latest master with Qt support.
6. Install vcpkg (required for installing protobuf)
```
	git clone https://github.com/Microsoft/vcpkg.git
	cd vcpkg
	.\bootstrap-vcpkg.bat
	.\vcpkg integrate install
```
7. Install protobuf using vcpkg:  
	`vcpkg install protobuf protobuf:x64-windows`  
  add `"...vcpkg/installed/x64-windows/share"` to path  

8. Build and install libzmq (ZeroMQ Library) using CMake.  

9. Build and install cppzmq (c++ wrapper for ZeroMQ Library) using CMake.  

10. Build async-sm  
	`git clone https://gitlab.kitware.com/utkarsh.ayachit/async-sm.git`  
	`git submodule init`  
	`git submodule update`  
	Build using CMake.  

After installation of each library, add the installation directories (bin, lib, share) to path if required.

ServerManager 3.0
=================



Our implementation follows a layered design.

The lowest layer is the communication layer (or `comm` namespace) that
handles communication between various components in our design.

comm
-----


[`comm`](./comm/comm.h) namespace encapsulates low-level communication components. These define how
inter-process (rather inter-service) communication is modelled. It also defines
the patterns available for asynchronous code execution.

`comm` comprises for three main components: services, messages, and broker.

**Service**s are components that ask for work to be done or do the work. Thus, in
traditional ParaView sense, both client and server are modelled as services.
There's no distinction between the two and similar mechanisms are available to
both.

All data is exchanged between services in form of **Message**s. A message
comprises for individual parts called frame.

**Broker** is an intermediary or junction that handles communication between all
services. It routes messages to appropriate services, and handles a small subset of messages
itself.

All the services and broker can be different processes or different threads on the
same process. (In theory, multiple services can be one the same thread, but we
will ignore that for now).

The infrastructure supports communication between services. `comm::Service`
provides API for this communication. In the following subsections,
we look at vaarious examples to demonstrate the capabilities.
Additional docs are available in the header for [`comm::Broker`](./comm/broker.h)
and [`comm::Service`](./comm/service.h).

Key take-aways of this layer are as follows:

1. This API is meant to be used by ServerManager.
2. It uses [stlab](http://stlab.cc/libraries/concurrency/) to make concurrency
   easier, [zeromq](http://zeromq.org/) to make inter-service communication
   easier, and [loguru](https://github.com/emilk/loguru) to make logging to the
   terminal easier (and more readable).
3. All services can communnicate with one another. In the past,
   server could only send messages to client if it was in collaboration mode,
   and even that relied on polling.  That's no longer the case.
4. All communication calls are non-blocking/asynchronous.
5. The handlers for async calls to handle responses or notifications can
   be provided a context or executor to execute in. Thus, it's just as easy
   to execute the handler in an independent thread from a thread pool or an
   event loop in the same thread as the one making the connections.


Request-Reply
--------------

This is a mechanism to send a request to service and get a response. First, let's look at the various ways of making
a request and processing the result.

```c++
service = .... // live comm::Service instance
dest   = .... // name of the destination service
message = ... // comm::Message instance with data frames to send.

// send a request without caring about when it is handled.
service.SendRequest(dest, std::move(message))
   .detach();

// send a request and wait for it to get a response.
auto f = service.SendRequest(dest, std::move(message));
Message reply = stlab::blocking_get(f);

// send a request and do some work on reply in the
// `event loop`.
using loop_executor = stlab::extensions::loop_executor_type;
auto f = service.SendRequest(dest, std::move(message))
           .then(loop_executor(), [](const comm::Message& msg) { ... });
f.detach(); // either detach or hold on to the stlab::future<T>
            // till you no longer care about it.

// send a request and do some work on an arbitrary thread
// from the thread pool.
auto f = service.SendRequest(dest, std::move(message))
           .then(stlab::default_executor, [](const comm::Message& msg) { ... });

// the default_executor is assumed when not provided, thus following
// would also have same effect.
auto f = service.SendRequest(dest, std::move(message))
           .then(stlab::default_executor, [](const comm::Message& msg) { ... });

// on reply, do some heavy, thread-safe work on some other thread from the thread pool
// to preprocess the reply and then do another task on the main even loop.
auto f = service.SendRequest(dest, std::move(message))
           .then(stlab::default_executor, [](const comm::Message& msg) { ...; return typename T; })
           .then(loop_executor(), [](const T& data){ ... });
```

Now, to repond to such requests, one must add handlers. This done by using `comm::Service::GetMainReceiver()`.
The methond returns a `stlab::receiver<Message>`. One can add callbacks to the receiver by using the `|` operator.


```c++
auto receiver = service.GetMainReceiver();

// here's an example callback that sends a reply.
auto callback = [&service=service](const comm::Message& incoming) {
    comm::Message reply;
    ...
    // to send a reply, one must pass the incoming message as the first
    // argument.
    service.SendReply(incoming, reply);
}

// Here's how to hookup the callback to be called on the event loop;
auto hold = receiver | stlab::executor(loop_executor()) & callback;
hold.detach(); // same as before, either detach or hold on to
               // instance till you no longer care about the callback.

// you can register mutliple callbacks.
auto hold2 = receiver | callback2;

// here, since no executor was provided, the `default_executor` will be used,
// which executes the `callback2` on a random thread from the thread pool.

// Here's another way of doing the same
auto hold = receiver | stlab::executor(loop_executor()) & callback
                     | callback2;

// once all `pipes` are setup, one must turn on the receiver.
receiver.set_ready();

// this must be done for all receivers i.e. if you call `GetMainReceiver` twice,
// each is a separate instance and must be turned on separately.
```

Request-No-Reply
--------------

A variation of the Request-Reply is to make a request but ignore the reply.
For such cases, use `comm::Service::SendMessage` instead of `comm::Service::SendRequest`. On the
receiving end, the same handlers are for `comm::Service::SendRequest` are invoked. Any calls
to `comm::Service::SendReply` in repose, however, are simply ignored.

```c++
service.SendMessage(dest, std::move(message));
```

Publish-Subscribe
-----------------

This is another mechanism to exchange messages. Services can subscribe to receive all messages on a named
channel. Conversely, a service can publish a message on a named channel and all subcribers
will receive that message.

To subscribe, use `comm::Service::Subscribe` which returns a `stlab::receiver<comm::Message>`. This is similar to
receiver returned by `GetMainReceiver` and hence supports similar capabilities. One difference is that
there are no replies expected, hence calling `comm::Service::SendReply` in the callback is an error.

```c++
auto channel_receiver = service.Subscribe("some-channel");
auto hold = channel_receiver | stlab::executor(loop_executor()) & callback3;
channel_receiver.set_ready();
```

To publish, use `comm::Service::Publish`.

```c++
comm::Message message = ...; // message to publish.
service.Publish("some-channel", std::move(message));
```

Message Order
-------------

Here are some important aspects to keep in mind about message order when issues calls that
send a message to another service.

1. When multiple SendRequest, Subscribe, and Publish calls are issued the
   corresponding messages will be dispatched to the broker (and target service)
   in the same order that they are issued.
2. The messages will be processed on the destination service in the same order as
   well. However this does not imply that the destination service is required to do
   the requested work or send a reply back in the same order.
