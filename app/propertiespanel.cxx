#include "propertiespanel.h"
#include "ui_propertiespanel.h"

#include <QDoubleValidator>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QIntValidator>
#include <QLabel>
#include <QLineEdit>
#include <QPointer>
#include <QVBoxLayout>

#include <comm/loop_executor.h>
#include <sm/client/proxy.h>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <set>

namespace app
{
namespace detail
{
class LineEdit : public QLineEdit
{
public:
  LineEdit(QWidget* p)
    : QLineEdit(p)
  {
  }
  QSize sizeHint() const override { return QSize(); }
};
}

class PropertiesPanel::PInternals
{
public:
  QPointer<QWidget> Container;
  std::shared_ptr<sm::client::Proxy> Proxy;
  Ui::PropertiesPanel Ui;
  stlab::future<void> UpdateInformationFuture;
};

//-----------------------------------------------------------------------------
PropertiesPanel::PropertiesPanel(QWidget* p, Qt::WindowFlags f)
  : Superclass(p, f)
  , Internals(new PropertiesPanel::PInternals())
{
  this->Internals->Ui.setupUi(this);
  auto v = new QVBoxLayout(this->Internals->Ui.propertiesContainer);
  v->setContentsMargins(0, 11, 0, 11);
}

//-----------------------------------------------------------------------------
PropertiesPanel::~PropertiesPanel()
{
}

//-----------------------------------------------------------------------------
void PropertiesPanel::pipelineUpdated(std::uint64_t gid)
{
  auto& internals = (*this->Internals);
  if (internals.Proxy && internals.Proxy->GetGlobalID() == gid)
  {
    internals.UpdateInformationFuture.reset();
    internals.UpdateInformationFuture =
      internals.Proxy->GetDataInformation().then(comm::loop_executor_type{},
        [& ui = internals.Ui](const std::string& info) {
          ui.informationLabel->setText(info.c_str());
        });
  }
}

//-----------------------------------------------------------------------------
std::shared_ptr<sm::client::Proxy> PropertiesPanel::showing() const
{
  return this->Internals->Proxy;
}

//-----------------------------------------------------------------------------
void PropertiesPanel::show(std::shared_ptr<sm::client::Proxy> proxy)
{
  auto& internals = (*this->Internals);
  if (internals.Proxy == proxy)
  {
    return;
  }
  internals.Proxy = proxy;
  internals.UpdateInformationFuture.reset();
  internals.Ui.informationLabel->setText("");
  if (!proxy)
  {
    delete internals.Container;
    return;
  }

  this->pipelineUpdated(proxy->GetGlobalID());

  auto oldContainer = internals.Container;
  internals.Container = new QWidget(this);

  auto l = new QGridLayout(internals.Container);
  l->setMargin(0);
  l->setHorizontalSpacing(4);
  l->setVerticalSpacing(4);
  l->setColumnStretch(0, 0);
  l->setColumnStretch(1, 1);

  // since the properties are not ordered, we sort them.
  std::set<std::string> keys;
  for (const auto& pair : proxy->GetState().properties())
  {
    keys.insert(pair.first);
  }

  int row = 0;
  for (const auto& pname : keys)
  {
    const auto& pstate = proxy->GetState().properties().at(pname);

    auto glayout = new QGridLayout();
    glayout->setMargin(0);
    glayout->setHorizontalSpacing(2);
    glayout->setVerticalSpacing(2);

    int value_index = 0;
    const auto value_size = pstate.values().size();
    for (const auto& value : pstate.values())
    {
      auto edit = new detail::LineEdit(internals.Container);
      switch (value.value_case())
      {
        case sm::common::Variant::kFloat64:
          edit->setValidator(new QDoubleValidator(edit));
          edit->setText(QString::number(value.float64()));
          break;
        case sm::common::Variant::kFloat32:
          edit->setValidator(new QDoubleValidator(edit));
          edit->setText(QString::number(value.float32()));
          break;
        case sm::common::Variant::kInteger:
          edit->setValidator(new QIntValidator(edit));
          edit->setText(QString::number(value.integer()));
          break;

        case sm::common::Variant::kStr:
          edit->setText(value.str().c_str());
          break;

        default:
          delete glayout;
          glayout = nullptr;
          delete edit;
          edit = nullptr;
          break;
      }

      if (edit == nullptr || glayout == nullptr)
      {
        break;
      }

      auto change_flag = std::make_shared<bool>(false);
      QObject::connect(edit, &QLineEdit::textEdited, [change_flag]() { *change_flag = true; });
      QObject::connect(
        edit, &QLineEdit::editingFinished, [this, change_flag, edit, proxy, pname, value_index]() {
          if (*change_flag == true)
          {
            auto& value =
              (*proxy->GetState().mutable_properties()->at(pname).mutable_values(value_index));
            switch (value.value_case())
            {
              case sm::common::Variant::kFloat64:
                value.set_float64(edit->text().toDouble());
                break;
              case sm::common::Variant::kFloat32:
                value.set_float32(edit->text().toFloat());
                break;
              case sm::common::Variant::kInteger:
                value.set_integer(edit->text().toInt());
                break;
              case sm::common::Variant::kStr:
                value.set_str(edit->text().toLatin1().data());
                break;
              default:
                break;
            }
            *change_flag = false;
            proxy->UpdateVTKObjects();
            emit this->changeFinished();
          }
        });
      if (value_size <= 3)
      {
        glayout->addWidget(edit, value_index / 3, value_index % 3);
      }
      else
      {
        glayout->addWidget(edit, value_index / 2, value_index % 2);
      }
      ++value_index;
    }

    if (glayout != nullptr)
    {
      auto label = new QLabel(pname.c_str(), internals.Container);
      l->addWidget(label, row, 0);
      l->addLayout(glayout, row, 1);
      row++;
    }
  }
  delete oldContainer;

  auto& ui = this->Internals->Ui;
  auto& frame = ui.propertiesContainer;
  ui.icon->setPixmap(QPixmap(proxy->GetIcon().c_str()));
  frame->layout()->addWidget(internals.Container);
}
}
