#ifndef app_mainwindow_h
#define app_mainwindow_h

#include <QMainWindow>
#include <memory>

namespace app
{
class MainWindow : public QMainWindow
{
  Q_OBJECT;

public:
  MainWindow(QWidget* prnt = nullptr, Qt::WindowFlags flgs = 0);
  ~MainWindow();

  bool connect(const std::string& brokerurl);

private:
  Q_DISABLE_COPY(MainWindow);

  class MInternals;
  std::unique_ptr<MInternals> Internals;
};
}

#endif
