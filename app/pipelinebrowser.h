#ifndef app_pipelinebrowser_h
#define app_pipelinebrowser_h

#include <QWidget>

#include <memory>

namespace sm
{
namespace client
{
class Proxy;
}
}

namespace app
{

class PipelineBrowser : public QWidget
{
  Q_OBJECT;
  using Superclass = QWidget;

public:
  PipelineBrowser(QWidget* parent = nullptr, Qt::WindowFlags f = 0);
  ~PipelineBrowser() override;

  void add(std::shared_ptr<sm::client::Proxy> proxy);
  void remove(std::shared_ptr<sm::client::Proxy> proxy);

  void select(std::shared_ptr<sm::client::Proxy> proxy);
signals:
  void selected(std::shared_ptr<sm::client::Proxy> proxy);

private:
  Q_DISABLE_COPY(PipelineBrowser);
  class PInternals;
  std::unique_ptr<PInternals> Internals;
};
}

#endif
