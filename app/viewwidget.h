#ifndef app_viewwidget_h
#define app_viewwidget_h

#include <QWidget>
#include <memory>

namespace sm
{
namespace client
{
class RenderViewProxy;
}
}

namespace app
{

class ViewWidget : public QWidget
{
  Q_OBJECT;
  using Superclass = QWidget;

public:
  ViewWidget(std::shared_ptr<sm::client::RenderViewProxy> rv, QWidget* parent = nullptr,
    Qt::WindowFlags f = 0);
  ~ViewWidget();

private:
  Q_DISABLE_COPY(ViewWidget);
  std::shared_ptr<sm::client::RenderViewProxy> ViewProxy;
};
}
#endif
