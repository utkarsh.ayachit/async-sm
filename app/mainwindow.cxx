#include "mainwindow.h"
#include "ui_paraview.h"

#include "viewwidget.h"

#include <comm/loop_executor.h>
#include <comm/message.h>
#include <comm/zmq.h>
#include <sm/client/proxy.h>
#include <sm/client/renderviewproxy.h>
#include <sm/client/session.h>
#include <sm/common/proxy.pb.h>
#include <sm/server/session.h>

#include <QMessageBox>
#include <QPointer>
#include <QProgressBar>
#include <QTimer>

#include <chrono>

#include <vtkAutoInit.h>
VTK_MODULE_INIT(vtkRenderingOpenGL2)
VTK_MODULE_INIT(vtkInteractionStyle)
VTK_MODULE_INIT(vtkRenderingFreeType)

namespace app
{

namespace detail
{

std::shared_ptr<sm::client::Proxy> show(
  std::shared_ptr<sm::client::Proxy> source, std::shared_ptr<sm::client::RenderViewProxy> view)
{
  // ensure it's created.
  source->UpdateVTKObjects();

  auto session = source->GetSession();
  auto reprproxy =
    sm::client::Proxy::createProxy(sm::client::Proxy::REPRESENTATION_GEOMETRY, session);
  reprproxy->setInput(source);

  (*view->GetState().mutable_properties())["Representations"].add_values()->set_proxy(
    reprproxy->GetGlobalID());
  view->UpdateVTKObjects();

  // since data modified, request update.
  view->Update();
  return reprproxy;
}

std::shared_ptr<sm::client::Proxy> create(sm::client::Proxy::KnownProxies type,
  std::shared_ptr<sm::client::Session> session, std::shared_ptr<sm::client::Proxy> input = nullptr)
{
  auto proxy = sm::client::Proxy::createProxy(type, session);
  if (input)
  {
    proxy->setInput(input);
  }
  proxy->UpdateVTKObjects();
  //  proxy->UpdatePipeline(1.0);
  return proxy;
}
}

class MainWindow::MInternals
{
public:
  Ui::MainWindow Ui;
  QPointer<QProgressBar> ProgressBar;
  std::shared_ptr<sm::client::Session> Session;
  std::vector<std::shared_ptr<sm::client::Proxy> > Proxies;
  stlab::receiver<void> ProgressBarReceiver;
  stlab::receiver<void> LiveUpdatesReceiver;
  stlab::receiver<void> PipelineUpdatesReceiver;
  std::shared_ptr<sm::client::RenderViewProxy> ViewProxy;

  std::shared_ptr<sm::client::Proxy> create(
    sm::client::Proxy::KnownProxies type, std::shared_ptr<sm::client::Proxy> input = nullptr)
  {
    auto proxy = detail::create(type, this->Session, input);
    this->Proxies.push_back(proxy);
    this->Ui.pipelineBrowser->add(proxy);
    this->Ui.pipelineBrowser->select(proxy);
    return proxy;
  }

  std::shared_ptr<sm::client::Proxy> show(std::shared_ptr<sm::client::Proxy> proxy)
  {
    auto repr = detail::show(proxy, this->ViewProxy);
    this->Proxies.push_back(repr);
    return repr;
  }

  std::shared_ptr<sm::client::Proxy> createAndShow(sm::client::Proxy::KnownProxies type)
  {
    return this->show(this->create(type));
  }
};

//-----------------------------------------------------------------------------
MainWindow::MainWindow(QWidget* prnt, Qt::WindowFlags flgs)
  : QMainWindow(prnt, flgs)
  , Internals(new MainWindow::MInternals())
{
  auto& internals = (*this->Internals);
  internals.Ui.setupUi(this);
  internals.ProgressBar = new QProgressBar();
  internals.ProgressBar->setMinimum(0);
  internals.ProgressBar->setMaximum(100);
  internals.Ui.statusbar->addPermanentWidget(internals.ProgressBar);

  QObject::connect(
    internals.Ui.actionSphereSource, &QAction::triggered, [pinternals = this->Internals.get()]() {
      pinternals->createAndShow(sm::client::Proxy::SPHERE);
    });

  QObject::connect(
    internals.Ui.actionWavelet, &QAction::triggered, [pinternals = this->Internals.get()]() {
      pinternals->createAndShow(sm::client::Proxy::WAVELET);
    });

  QObject::connect(
    internals.Ui.actionContour, &QAction::triggered, [pinternals = this->Internals.get()]() {
      auto current = pinternals->Ui.propertiesPanel->showing();
      auto contour = pinternals->create(sm::client::Proxy::CONTOUR, current);
      contour->UpdateVTKObjects();
      pinternals->show(contour);
    });

  QObject::connect(
    internals.Ui.actionSlice, &QAction::triggered, [pinternals = this->Internals.get()]() {
      auto current = pinternals->Ui.propertiesPanel->showing();
      auto slice = pinternals->create(sm::client::Proxy::SLICE, current);
      slice->UpdateVTKObjects();
      pinternals->show(slice);
    });

  QObject::connect(
    internals.Ui.actionWaveletLive, &QAction::triggered, [pinternals = this->Internals.get()]() {
      auto repr = pinternals->createAndShow(sm::client::Proxy::LIVE);
      auto& props = *repr->GetState().mutable_properties();
      props["ShowTimeAnnotation"].mutable_values(0)->set_integer(1);
      repr->UpdateVTKObjects();
    });

  QObject::connect(internals.Ui.actionSenseiLive, &QAction::triggered,
    [ this, pinternals = this->Internals.get() ]() {
#if defined(ENABLE_SENSEI)
      auto repr = pinternals->createAndShow(sm::client::Proxy::SENSEILIVE);
      auto& props = *repr->GetState().mutable_properties();
      props["ShowTimeAnnotation"].mutable_values(0)->set_integer(1);
      repr->UpdateVTKObjects();
#else
      QMessageBox::critical(
        this, "SENSEI Live not available", "Please rebuild with SENSEI enabled.");
#endif
    });

  QObject::connect(
    internals.Ui.actionManySpheres, &QAction::triggered, [pinternals = this->Internals.get()]() {
      LOG_S(INFO) << "create 10 sphere source";
      for (int cc = 0; cc < 10; ++cc)
      {
        static int counter = 0;
        auto sphere = pinternals->create(sm::client::Proxy::SPHERE);
        auto& props = (*sphere->GetState().mutable_properties());
        props["Center"].mutable_values(0)->set_float64(counter % 10);
        props["Center"].mutable_values(1)->set_float64(counter / 10);
        counter++;

        sphere->UpdateVTKObjects();
        pinternals->show(sphere);
      }
      LOG_S(INFO) << "create 10 sphere source -- done";
    });

  // whenever a change is available, we need to update the view.
  QObject::connect(internals.Ui.propertiesPanel, &app::PropertiesPanel::changeFinished,
    [this]() { this->Internals->ViewProxy->Update(); });

  QObject::connect(internals.Ui.pipelineBrowser, &app::PipelineBrowser::selected,
    [this](std::shared_ptr<sm::client::Proxy> proxy) {
      this->Internals->Ui.propertiesPanel->show(proxy);
    });

  QObject::connect(internals.Ui.actionResetCamera, &QAction::triggered,
    [& view = this->Internals->ViewProxy]() { view->ResetCamera(); });

  QTimer* yawTimer = new QTimer(this);
  yawTimer->setInterval(10);
  yawTimer->setSingleShot(false);
  QObject::connect(internals.Ui.actionSpin, &QAction::triggered, [yawTimer](bool checked) {
    if (checked)
    {
      yawTimer->start();
    }
    else
    {
      yawTimer->stop();
    }
  });
  QObject::connect(yawTimer, &QTimer::timeout, [& view = this->Internals->ViewProxy]() {
    if (QApplication::mouseButtons() == Qt::NoButton)
    {
      view->Azimuth(1);
    }
  });
}

//-----------------------------------------------------------------------------
MainWindow::~MainWindow()
{
  // FIXME:
  // auto& internals = (*this->Internals);
  // internals.Session->GetService().TerminateAll();
}

//-----------------------------------------------------------------------------
namespace detail
{

// since we don't care about too many progress events,
// we use this throttler. it allows us to skip progress events
// sooner than 2hz except for `100` which indicates progress end.
struct progress_throttler
{
  void await(int progress)
  {
    this->progress = progress;
    if (progress == 100)
    {
      this->_state = stlab::yield_immediate;
    }
    else
    {
      auto now = std::chrono::steady_clock::now();
      auto delta =
        std::chrono::duration_cast<std::chrono::milliseconds>(now - this->last_event_time).count();
      if (delta >= 200)
      {
        this->_state = stlab::yield_immediate;
      }
      else
      {
        this->_state = stlab::await_forever;
      }
    }
  }

  int yield()
  {
    this->last_event_time = std::chrono::steady_clock::now();
    this->_state = stlab::await_forever;
    return this->progress;
  }

  auto state() const { return this->_state; }

private:
  stlab::process_state_scheduled _state = stlab::await_forever;
  std::chrono::steady_clock::time_point last_event_time = std::chrono::steady_clock::now();
  int progress;
};
}

//-----------------------------------------------------------------------------
bool MainWindow::connect(const std::string& burl)
{
  auto& internals = (*this->Internals);
  internals.Session = std::make_shared<sm::client::Session>();
  if (internals.Session->Initialize(burl))
  {
    auto executor = comm::loop_executor_type{};
    auto recvr = internals.Session->GetService().subscribe("progress");

    std::atomic_int progress_value(0);
    internals.ProgressBarReceiver = recvr |
      [](std::shared_ptr<const comm::Message> msg) {
        sm::common::ProxyProgress pstate;
        if (msg->size() == 1 && comm::z2p(msg->back(), &pstate))
        {
          return static_cast<int>(100 * pstate.value());
        }
        else
        {
          return 100;
        }
      } |
      detail::progress_throttler{} |
      stlab::executor(executor) & [progressbar = internals.ProgressBar](const int& val) {
        if (val >= 100)
        {
          progressbar->reset();
        }
        else
        {
          progressbar->setValue(val);
        }
      };
    recvr.set_ready();

    // subcribe for live updates.
    auto lu_recvr = internals.Session->GetService().subscribe("liveupdate");
    internals.LiveUpdatesReceiver =
      lu_recvr | stlab::executor(executor) & [this](std::shared_ptr<const comm::Message>) {
        // LOG_S(INFO) << "got live update";
        this->Internals->ViewProxy->Update();
      };
    lu_recvr.set_ready();

    // create view.
    internals.ViewProxy = std::make_shared<sm::client::RenderViewProxy>(internals.Session);
    ViewWidget* v = new ViewWidget(internals.ViewProxy, this);
    this->setCentralWidget(v);

    // monitor "pipeline-updated" to update data information, as needed.
    auto pu_recvr = internals.Session->GetService().subscribe("pipeline-updated");
    internals.PipelineUpdatesReceiver = pu_recvr |
      stlab::executor(executor) & [& ui = internals.Ui](std::shared_ptr<const comm::Message> msg) {
        sm::common::ProxyPipelineUpdated pg;
        comm::z2p(msg->front(), &pg);
        ui.propertiesPanel->pipelineUpdated(pg.global_id());
      };
    pu_recvr.set_ready();
    return true;
  }
  return false;
}

}
