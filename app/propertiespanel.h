#ifndef app_propertiespanel_h
#define app_propertiespanel_h

#include <QWidget>

#include <memory>

namespace sm
{
namespace client
{
class Proxy;
}
}

namespace app
{

class PropertiesPanel : public QWidget
{
  Q_OBJECT;
  using Superclass = QWidget;

public:
  PropertiesPanel(QWidget* parent = nullptr, Qt::WindowFlags f = 0);
  ~PropertiesPanel() override;

  void show(std::shared_ptr<sm::client::Proxy> proxy);
  std::shared_ptr<sm::client::Proxy> showing() const;

  void pipelineUpdated(std::uint64_t gid);

signals:
  void changeFinished();

private:
  Q_DISABLE_COPY(PropertiesPanel);
  class PInternals;
  std::unique_ptr<PInternals> Internals;
};
}

#endif
