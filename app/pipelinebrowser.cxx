#include "pipelinebrowser.h"

#include <sm/client/proxy.h>

#include <QHeaderView>
#include <QPointer>
#include <QStandardItemModel>
#include <QTreeView>
#include <QVBoxLayout>

namespace app
{

class PipelineBrowser::PInternals
{
public:
  QPointer<QTreeView> View;
  QPointer<QStandardItemModel> Model;
  std::map<std::uint64_t, std::shared_ptr<sm::client::Proxy> > Proxies;
  std::map<std::uint64_t, std::pair<QStandardItem*, QStandardItem*> > ProxyItems;
  ;

  PInternals()
    : View(new QTreeView())
    , Model(new QStandardItemModel())
  {
    this->Model->setColumnCount(2);
    this->View->setModel(this->Model);
    this->View->setRootIsDecorated(true);
    this->View->header()->moveSection(0, 1);
    this->View->header()->resizeSection(1, 10);
    this->View->header()->setStretchLastSection(true);
    this->View->header()->hide();
    this->View->setUniformRowHeights(true);
    this->View->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->View->setSelectionMode(QAbstractItemView::SingleSelection);
  }
};

//-----------------------------------------------------------------------------
PipelineBrowser::PipelineBrowser(QWidget* p, Qt::WindowFlags f)
  : Superclass(p, f)
  , Internals(new PipelineBrowser::PInternals())
{
  auto l = new QVBoxLayout(this);
  l->setMargin(0);
  l->addWidget(this->Internals->View);

  QObject::connect(this->Internals->View->selectionModel(), &QItemSelectionModel::currentChanged,
    [this](const QModelIndex& current, const QModelIndex&) {
      if (auto item = this->Internals->Model->itemFromIndex(current))
      {
        std::uint64_t gid = item->data().value<std::uint64_t>();
        auto iter = this->Internals->Proxies.find(gid);
        if (iter != this->Internals->Proxies.end())
        {
          emit this->selected(iter->second);
          return;
        }
      }
      emit this->selected(nullptr);
    });
}

//-----------------------------------------------------------------------------
PipelineBrowser::~PipelineBrowser()
{
}

//-----------------------------------------------------------------------------
void PipelineBrowser::add(std::shared_ptr<sm::client::Proxy> proxy)
{
  const auto gid = proxy->GetGlobalID();
  this->Internals->Proxies[gid] = proxy;

  auto model = this->Internals->Model;
  auto root = model->invisibleRootItem();

  if (proxy->input() != nullptr)
  {
    const auto inputGID = proxy->input()->GetGlobalID();
    root = this->Internals->ProxyItems.at(inputGID).first;
  }

  auto item0 = new QStandardItem(proxy->GetState().xml_name().c_str());
  item0->setFlags(item0->flags() & ~Qt::ItemIsEditable);
  item0->setData(QSize(0, 26), Qt::SizeHintRole);
  item0->setData(QVariant::fromValue(gid));
  if (!proxy->GetIcon().empty())
  {
    item0->setIcon(QIcon(proxy->GetIcon().c_str()));
  }

  auto item1 = new QStandardItem();
  item1->setFlags(item1->flags() & ~Qt::ItemIsEditable);
  item1->setData(QVariant::fromValue(gid));
  item1->setIcon(QIcon(":/paraview/icons/baseline_visibility_black_24dp.png"));

  root->appendRow({ item0, item1 });

  this->Internals->ProxyItems[gid] = std::make_pair(item0, item1);
}

//-----------------------------------------------------------------------------
void PipelineBrowser::select(std::shared_ptr<sm::client::Proxy> proxy)
{
  auto& internals = *this->Internals;
  const auto gid = proxy ? proxy->GetGlobalID() : 0;
  auto iter = internals.ProxyItems.find(gid);
  if (iter != internals.ProxyItems.end())
  {
    internals.View->selectionModel()->setCurrentIndex(
      iter->second.first->index(), QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
  }
  else
  {
    internals.View->selectionModel()->clear();
  }
}

//-----------------------------------------------------------------------------
void PipelineBrowser::remove(std::shared_ptr<sm::client::Proxy> proxy)
{
  // todo: not supported.
}
}
