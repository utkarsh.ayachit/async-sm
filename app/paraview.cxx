#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <QSurfaceFormat>
#include <QVTKOpenGLNativeWidget.h>
#include <chrono>
#include <comm/broker.h>
#include <comm/loop_executor.h>
#include <comm/zmq.h>
#include <future>
#include <sm/client/proxy.h>
#include <sm/client/session.h>
#include <sm/common/proxy.pb.h>
#include <sm/server/session.h>

#include "mainwindow.h"
#include <QApplication>
#include <QEvent>
#include <QThread>
#include <QTimer>

struct queued_closures_runner : public QObject
{
  bool event(QEvent*) override
  {
    auto executor = comm::loop_executor_type{};
    executor.run_queued_closures();
    this->enqueue();
    return true;
  }
  void enqueue()
  {
    auto event = std::make_unique<QEvent>(QEvent::User);
    QApplication::postEvent(this, event.release());
  }
};

#if defined(ENABLE_SENSEI)
#include <mpi.h>
#endif

int main(int argc, char* argv[])
{
#if defined(ENABLE_SENSEI)
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
#endif

  // Turn off individual parts of the preamble
  loguru::g_preamble_date = false; // The date field
  loguru::g_preamble_time = false; // The time of the current day
  loguru::add_file("latest_readable.log", loguru::Truncate, 6);
  loguru::init(argc, argv);
  LOG_S(INFO) << "Starting process...";

  // init zero mq.
  comm::zmq_initialize();

  //---------------------------------------------------------------------------
  // BROKER
  //---------------------------------------------------------------------------
  auto retvalB = std::async(std::launch::async, []() {
    comm::Broker broker;
    // NOTE: don't forget to fix
    // vtkextensions::pack_vtk_object/extract_vtk_object
    // before changing to use tcp.
    return broker.exec("inproc://paraview-channel");
  });
  using namespace std::chrono_literals;
  std::this_thread::sleep_for(1s);

  //---------------------------------------------------------------------------
  // DATA-SERVER
  //---------------------------------------------------------------------------
  auto retvalDS = std::async(std::launch::async, []() {
    sm::server::Session session;
    session.Initialize("inproc://paraview-channel", "ds");
    auto executor = comm::loop_executor_type{};
    LOG_S(INFO) << "eid (ds) = " << executor.id();
    executor.loop();
  });

  //---------------------------------------------------------------------------
  // RENDER-SERVER
  //---------------------------------------------------------------------------
  auto retvalRS = std::async(std::launch::async, []() {
    sm::server::Session session;
    session.Initialize("inproc://paraview-channel", "rs");
    auto executor = comm::loop_executor_type{};
    LOG_S(INFO) << "eid (rs) = " << executor.id();
    executor.loop();
  });

  // TODO: RENDER-SERVER-LITE

  //---------------------------------------------------------------------------
  // CLIENT
  //---------------------------------------------------------------------------
  QSurfaceFormat::setDefaultFormat(QVTKOpenGLNativeWidget::defaultFormat());
  QApplication app(argc, argv);
  app.setAttribute(Qt::AA_UseHighDpiPixmaps);
  app::MainWindow window;
  window.connect("inproc://paraview-channel");
  window.show();

#if 0
  // a little ugly hack. we need to tell client::session the type of executor to
  // use for its async calls and then make that be the `stlab::main_executor`
  // which uses the Qt main loop instead.
  QTimer timer;
  timer.setSingleShot(false);
  timer.setInterval(0);
  QObject::connect(&timer, &QTimer::timeout, []() {
    auto executor = comm::loop_executor_type{};
    LOG_S(INFO) << "run_queued_closures";
    executor.run_queued_closures();
  });
  timer.start();
#else
  // this works better on OsX than timers. Timers seem to be deferred when
  // active interaction is in progress and can cause the rendering to get stuck
  // when interacting.
  queued_closures_runner runner;
  runner.enqueue();
#endif

  int r = app.exec();
  LOG_S(INFO) << "qt-done";
  comm::zmq_finalize();

#if defined(ENABLE_SENSEI)
  MPI_Finalize();
#endif
  return r;
}
