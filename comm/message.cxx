#include "message.h"

#include <sstream>

namespace comm
{

//-----------------------------------------------------------------------------
void Message::push_back(Message&& msg)
{
  for (auto& aframe : msg)
  {
    this->push_back(std::move(aframe));
  }
  msg.clear();
}

//-----------------------------------------------------------------------------
void Message::empty_back()
{
  this->push_back(Frame(size_t(0)));
}

//-----------------------------------------------------------------------------
void Message::clone_back(const Frame& msg)
{
  Frame clone;
  clone.copy(&msg);
  this->push_back(std::move(clone));
}

//-----------------------------------------------------------------------------
void Message::clone_back(const Message& msg)
{
  for (const auto& aframe : msg)
  {
    this->clone_back(aframe);
  }
}

//-----------------------------------------------------------------------------
Message Message::clone(Message::const_iterator b, Message::const_iterator e)
{
  Message clone;
  for (auto iter = b; iter != e; ++iter)
  {
    clone.clone_back(*iter);
  }
  return clone;
}

//-----------------------------------------------------------------------------
void Message::clear_data(bool remove_delimiter)
{
  for (auto iter = this->begin(); iter != this->end(); ++iter)
  {
    if (iter->size() == 0)
    {
      if (!remove_delimiter)
      {
        ++iter;
      }
      if (iter != this->end())
      {
        this->erase(iter, this->end());
      }
      break;
    }
  }
}

//-----------------------------------------------------------------------------
void Message::clear_envelope(bool remove_delimiter)
{
  auto ipair = this->get_envelope();
  if (remove_delimiter && ipair.second != this->end())
  {
    ipair.second++;
  }
  this->erase(ipair.first, ipair.second);
}

//-----------------------------------------------------------------------------
std::pair<Message::const_iterator, Message::const_iterator> Message::get_envelope() const
{
  return std::make_pair(this->begin(),
    std::find_if(this->begin(), this->end(), [](const Frame& msg) { return msg.size() == 0; }));
}

//-----------------------------------------------------------------------------
std::pair<Message::iterator, Message::iterator> Message::get_envelope()
{
  return std::make_pair(this->begin(),
    std::find_if(this->begin(), this->end(), [](const Frame& msg) { return msg.size() == 0; }));
}

//-----------------------------------------------------------------------------
std::string Message::get_debug_string() const
{
  std::ostringstream str;
  str << "Message [ ";
  for (const auto& aframe : *this)
  {
    str << aframe.size() << " ";
  }
  str << "]";
  return str.str();
}

} // namespace comm
