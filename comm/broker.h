#ifndef comm_broker_h
#define comm_broker_h

#include <memory>
#include <string>

namespace comm
{

/**
 * @class Broker
 * @brief communication junction
 *
 * The Broker is the communication junction that routes all messages between
 * various services. Services "sign up" with the broken and then send/receive
 * messages. There must be exactly one broker in our network topology.
 *
 * `Broker::Exec` is the main method that starts the broker. The method blocks
 * until the `end` (todo: define the end criteria). Thus, the method should be
 * called on the thread that is to act as the broker.
 *
 * The broker *binds* to a **ROUTER** socket using the URL passed to `Exec`.
 * All services connect to this socket and exchange messages. Every incoming message
 * is either processed by the broker itself or routed to some other service
 * based on the following rules.
 *
 * Every message (comm::Message) received comprises is multiple frames of type
 * `comm::Frame`. The last frame is called a footer and holds the binary
 * representation for `comm::Footer`. Every message **must** have a valid
 * footer. Messages with invalid footer are ignored. The type for the footer is
 * encoded in it's `body`. Following are the list of supported footer.body types
 * and corresponding actions that the broker takes.
 *
 * 1. `comm::GreetingRequest`: this indicates a new service is joining. The Broker
 *      registers the service and replies with a message with
 *      comm::GreetingResponse in the footer indicating the success or failure.
 *      In future, this will be used to authenticate the connection.
 *      Current implementation supports multiple services with same name, in
 *      future, we may limit that.
 *
 * 2. `comm::GenericRequest`: this indicate a request message that is forwarded
 *      to the services named in `comm::GenericRequest.destinations`. If at least
 *      one service identified in the destinations is known, the message is
 *      forwarded to the appropriate service. If no service is found a
 *      `comm::GenericResponse` footer message is sent back with
 *      `comm::GenericResponse.source` set to "broker" and
 *      `comm::GenericResponse.status` set to false.
 *
 * 3. `comm::GenericResponse`: this indicates this a response to a request
 *      message received earlier. The broker removes the incoming message
 *      envelope and then dispatches that. The response has a second envelope
 *      that has the routing information needed to route the message back to the
 *      requester.
 *
 * 4. `comm::Subscribe`: this indicates a request from a service to subscribe to
 *      a channel. There is no response to this request. The broker updates its
 *      internal data structure to track this subscriber.
 *
 * 5. `comm::Publish`: this indicates a request from a service to publish data
 *      to a channel. The broker replaces the message envelope with the
 *      subscriber envelope and dispatches that. This is done for all
 *      subscribers.
 */
class Broker
{
public:
  Broker();
  ~Broker();

  /// executes the broker in the current thread.
  /// this call will block until the broker is 'done' (currently, that's
  /// forever, in future, it will terminate gracefully).
  int exec(const std::string& url);

private:
  Broker(const Broker&) = delete;
  void operator=(const Broker&) = delete;

  class BInternals;
  std::unique_ptr<BInternals> Internals;
};
}

#endif
