#include "serialization_pb.h"

#include "pipe.h"

#include "../message.h"

#include <condition_variable>
#include <future>
#include <memory>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <stlab/concurrency/default_executor.hpp>
#include <stlab/concurrency/utility.hpp>

namespace comm
{
namespace internal
{
static constexpr int32_t UNSOLICTED_MESSAGE = 0;

class ConnectionStatusInfo
{
  std::mutex Mutex;
  std::condition_variable CV;
  bool StatusIsSet = false;
  bool Status = false;

public:
  void set_status(bool val)
  {
    std::unique_lock<std::mutex> lk(this->Mutex);
    this->StatusIsSet = true;
    this->Status = val;
    lk.unlock();
    this->CV.notify_one();
  }

  bool wait_and_get_status()
  {
    std::unique_lock<std::mutex> lk(this->Mutex);
    this->CV.wait(lk, [this] { return this->StatusIsSet; });
    return this->Status;
  }
};

static inline std::int32_t callbackid_from_message(const comm::Message& msg)
{
  assert(msg.size() > 0 && msg.front().size() == sizeof(std::int32_t));
  return *reinterpret_cast<const std::uint64_t*>(msg.front().data());
}

static comm::Message add_frame(std::int32_t callbackid, comm::Message&& msg)
{
  comm::Message newmsg;
  newmsg.emplace_back(zmq::message_t(&callbackid, sizeof(callbackid)));
  newmsg.empty_back(); // header delimiter.
  newmsg.push_back(std::move(msg));
  return newmsg;
}

class PInternals
{
  PInternals(const PInternals&) = delete;
  void operator=(const PInternals&) = delete;

  std::mutex MutexForMapOfCallbacks;
  std::map<std::int32_t, std::function<bool(comm::Message&&)> > MapOfCallbacks;

  std::atomic<std::int32_t> NextId;

public:
  tbb::concurrent_queue<std::shared_ptr<comm::Message> > MessageQueue;
  std::atomic_bool Terminate;

  PInternals()
    : NextId{ 1 }
    , Terminate{ false }
  {
  }

  auto registerCallback(std::function<bool(comm::Message&&)>&& f)
  {
    auto id = ++this->NextId;
    std::lock_guard<std::mutex> guard(this->MutexForMapOfCallbacks);
    this->MapOfCallbacks.emplace(std::make_pair(id, std::move(f)));
    return id;
  }

  void enqueue(comm::Message&& msg)
  {
    this->MessageQueue.push(std::make_shared<comm::Message>(std::move(msg)));
  }

  void handle_message(comm::Message&& msg, std::function<void(comm::Message&&)>& callback0)
  {
    auto envelope = msg.get_envelope();
    const auto envelope_size = std::distance(envelope.first, envelope.second);
    LOG_S(1) << "handling message: " << msg.get_debug_string();
    if (envelope_size == 0)
    {
      // this is an message from the broker, typically a control message
      // or some other notification.
      LOG_S(1) << "got broker msg: " << msg.get_debug_string();
      return;
    }

    std::int32_t cid = UNSOLICTED_MESSAGE;
    if (envelope_size == 1)
    {
      // a message with 1-size envelope is response to an earlier message.
      cid = callbackid_from_message(msg);
      msg.clear_envelope_and_delimiter();
    }

    if (cid == UNSOLICTED_MESSAGE)
    {
      // an message with a large envelope indicates a request from a remote service.
      // dispatch to the callback0 and let it deal with it.
      LOG_S(1) << "dispatching: " << msg.get_debug_string();
      callback0(std::move(msg));
    }
    else
    {
      try
      {
        auto& callback = this->get_callback(cid);
        // callback returns `true` to indicate it is expected more messages,
        // if not, then we clean it up.
        if (callback(std::move(msg)) == false)
        {
          this->remove_callback(cid);
        }
      }
      catch (std::out_of_range)
      {
        LOG_S(ERROR) << "no callback found for cid: " << cid;
      }
    }
  }

private:
  std::function<bool(comm::Message&&)>& get_callback(std::int32_t cid)
  {
    std::lock_guard<std::mutex> guard(this->MutexForMapOfCallbacks);
    return this->MapOfCallbacks.at(cid);
  }

  void remove_callback(std::int32_t cid)
  {
    std::lock_guard<std::mutex> guard(this->MutexForMapOfCallbacks);
    this->MapOfCallbacks.erase(cid);
  }
};

//-----------------------------------------------------------------------------
static bool do_handshake(const std::string servicename, zmq::socket_t& ep_broker)
{
  comm::GreetingRequest greeting;
  greeting.set_service_name(servicename);

  comm::Footer footer;
  footer.mutable_body()->PackFrom(greeting);

  comm::Message frames;
  frames.empty_back();
  frames.push_back(footer);

  LOG_S(1) << "sending greeting.";
  comm::send(std::move(frames), ep_broker);
  // TODO: poll-n-wait with repeated "greetings" until success or failure.
  // TODO: do advanced handshake validating version numbers etc.
  // TODO: allow application to provide custom handshake strings
  frames = comm::recv(ep_broker);

  if (frames.size() == 0 || comm::z2p(frames.back(), &footer) == false ||
    !footer.body().Is<comm::GreetingResponse>())
  {
    LOG_S(ERROR) << "incorrect message received!";
    return false;
  }

  comm::GreetingResponse gres;
  footer.body().UnpackTo(&gres);
  LOG_S(1) << "greeting status: " << gres.status();
  return gres.status();
}

//-----------------------------------------------------------------------------
static bool do_goodbye(const std::string servicename, zmq::socket_t& ep_broker)
{
  // broadcast a good-bye message.
  comm::Goodbye term;
  term.set_service_name(servicename);

  comm::Footer footer;
  footer.mutable_body()->PackFrom(term);

  comm::Message frames;
  frames.empty_back();
  frames.push_back(footer);

  // send goodbye.
  comm::send(std::move(frames), ep_broker);
  return true;
}

//-----------------------------------------------------------------------------
// process message frames received on broker end point
static bool do_broker_message(zmq::socket_t& ep_broker,
  std::function<void(comm::Message&&)>& callback0,
  std::shared_ptr<PInternals>& internals)
{
  zmq::pollitem_t items[] = { { static_cast<void*>(ep_broker), 0, ZMQ_POLLIN, 0 } };
  while (zmq::poll(items, 1, 0) != -1)
  {
    if (items[0].revents & ZMQ_POLLIN)
    {
      internals->handle_message(comm::recv(ep_broker), callback0);
    }
    else
    {
      break;
    }
  }
  return true;
}

//-----------------------------------------------------------------------------
static bool do_flush_queue(zmq::socket_t& ep_broker, std::shared_ptr<PInternals>& internals)
{
  zmq::pollitem_t items[] = { { static_cast<void*>(ep_broker), 0, ZMQ_POLLOUT, 0 } };
  std::shared_ptr<comm::Message> msg;
  while (internals->MessageQueue.try_pop(msg))
  {
    if (zmq::poll(items, 1, 0) != -1 && items[0].revents & ZMQ_POLLOUT)
    {
      comm::send(std::move(*msg), ep_broker);
    }
  }
  return true;
}

//-----------------------------------------------------------------------------
static bool pipe_exec(const std::string servicename,
  const std::string brokerurl,
  std::function<void(comm::Message&&)> callback0,
  std::shared_ptr<ConnectionStatusInfo> cinfo,
  std::shared_ptr<comm::internal::PInternals> internals)
{
  loguru::set_thread_name(("pipe:" + servicename).c_str());

  // setup zmq sockets for all endpoints going out of the pipe thread.
  zmq::socket_t ep_broker(comm::get_zmq_context(), ZMQ_DEALER);
  ep_broker.setsockopt(ZMQ_LINGER, 0);
  ep_broker.setsockopt(ZMQ_IMMEDIATE, 1);
  ep_broker.setsockopt(ZMQ_SNDHWM, 500000);
  ep_broker.connect(brokerurl);

  // send greeting to broker and report status back via the cinfo.
  if (do_handshake(servicename, ep_broker))
  {
    cinfo->set_status(true);
  }
  else
  {
    cinfo->set_status(false);
    return false;
  }

  LOG_S(INFO) << "polling...";
  while (true)
  {
    do_flush_queue(ep_broker, internals);
    if (internals->Terminate)
    {
      LOG_S(INFO) << "terimation requested...";
      break;
    }

    // received a message from the broker.
    do_broker_message(ep_broker, callback0, internals);
  }
  do_goodbye(servicename, ep_broker);
  LOG_S(INFO) << "done...";
  return true;
}

//-----------------------------------------------------------------------------
Pipe::Pipe(const std::string& servicename, std::function<void(comm::Message&&)>&& callback0)
  : OwnerThreadId(std::this_thread::get_id())
  , ServiceName(servicename)
  , Callback0(std::move(callback0))
{
}

//-----------------------------------------------------------------------------
Pipe::~Pipe()
{
  CHECK_EQ_F(this->OwnerThreadId,
    std::this_thread::get_id(),
    "called on different thread than the owner!!!");
  this->close();
}

//-----------------------------------------------------------------------------
stlab::future<bool> Pipe::connect(const std::string& brokerurl)
{
  // TODO: what should happen if this method is called twice?
  // TODO: what should happen if this method is called after a previous
  //       connected pipe has terminated by error or success?
  CHECK_EQ_F(this->OwnerThreadId,
    std::this_thread::get_id(),
    "called on different thread than the owner!!!");

  auto cinfo = std::make_shared<ConnectionStatusInfo>();
  this->Internals = std::make_shared<PInternals>();

  // this deliberately uses std::async which doesn't occupy a thread from the
  // stlab thread pool.
  this->TaskStatus = std::async(std::launch::async,
    pipe_exec,
    this->ServiceName,
    brokerurl,
    this->Callback0,
    cinfo,
    this->Internals);

  // add an async task to get the connection status and return it.
  return stlab::async(
    stlab::immediate_executor, [cinfo]() { return cinfo->wait_and_get_status(); });
}

//-----------------------------------------------------------------------------
void Pipe::close()
{
  CHECK_EQ_F(this->OwnerThreadId,
    std::this_thread::get_id(),
    "called on different thread than the owner!!!");
  if (this->TaskStatus.valid())
  {
    this->Internals->Terminate = true;
    // wait for the async task to terminate.
    this->TaskStatus.get();
    LOG_S(INFO) << "pipe closed";
  }
}

//-----------------------------------------------------------------------------
void Pipe::enqueue(comm::Message&& msg)
{
  this->Internals->enqueue(add_frame(UNSOLICTED_MESSAGE, std::move(msg)));
}

//-----------------------------------------------------------------------------
void Pipe::enqueue_without_framing(comm::Message&& msg)
{
  this->Internals->enqueue(std::move(msg));
}

//-----------------------------------------------------------------------------
void Pipe::enqueue(comm::Message&& msg, std::function<bool(comm::Message&&)>&& f)
{
  auto id = this->Internals->registerCallback(std::move(f));
  this->Internals->enqueue(add_frame(id, std::move(msg)));
}

} // namespace comm::internal
} // namespace comm
