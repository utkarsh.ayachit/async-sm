#ifndef comm_internal_serialization_pb_h
#define comm_internal_serialization_pb_h

#include <zmq.hpp>
#include "comm.pb.h"

namespace comm
{
namespace serialization
{
void save(zmq::message_t& message_frame, const google::protobuf::Message& item);
} // namespace comm::serialization

template<typename T>
bool z2p(const zmq::message_t& msg, T* t)
{
  comm::MessageWrapper wrapper;
  if (wrapper.ParseFromArray(msg.data(), msg.size()))
  {
    if (wrapper.content().Is<T>())
    {
      return wrapper.content().UnpackTo(t);
    }
  }
  return false;
}
} // namespace comm
#endif
