#ifndef comm_internal_pipe_h
#define comm_internal_pipe_h

#include <future>
#include <memory>
#include <string>
#include <thread>

#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/future.hpp>
#include <tbb/concurrent_hash_map.h>
#include <tbb/concurrent_queue.h>

#include "../zmq.h"

namespace comm
{
namespace internal
{

class PInternals;
/**
 * @class Pipe
 * @brief Internal class to manage the communication pipe to broker from any
 *        service.
 *
 * Pipe helps a service communicate with the broker (through which it can
 * communicate to all other services). This class helps comm::Service provide
 * asynchronous APIs.
 *
 * None of this class' API is thread-safe. One should invoke all methods on the
 * same thread on which the class was instantiated. Internally, this class uses
 * a `std::async` task to handle all communication piping between the parent
 * service and the broker.
 *
 * This is an internal class and API is not meant for public consumption.
 */
class Pipe
{
public:
  /**
   * Creates a pipe. A pipe is not active until `Pipe::connect`.
   *
   * @param servicename name for the service to which the pipe belongs.
   * @param defaultCallback a callback function to invoke for all unsolicited messages.
   */
  Pipe(const std::string& servicename, std::function<void(comm::Message&&)>&& defaultCallback);

  /**
   * Closes the pipe. This will call `close()` to close any open connection.
   */
  ~Pipe();

  /**
   * Returns the service name.
   */
  const std::string& get_servicename() const { return this->ServiceName; }

  /**
   * Connect to the broker. This returns a future that indicates the status of
   * the connection. The future is not set until the broker has responded and
   * the handshake between the pipe and the broker is completed (either
   * successfully or in failure). The future indicates the state of the
   * handshake.
   *
   * @param brokerurl url for the broker.
   *
   * @returns future that is set to true if and when the connection succeeds
   *          to false otherwise.
   */
  stlab::future<bool> connect(const std::string& brokerurl);

  /**
   * Closes a connection previously established. Any queued or pending messages
   * may get lost. If connected to a broker, this sends a goodbye message
   * notifying the broker that the service that owns this pipe is going away.
   */
  void close();

  /**
   * Add messages to the queue to send to the broker. Use this overload when no
   * response is expected from the broker for this message.
   */
  void enqueue(comm::Message&& msg);

  /**
   * Add a message to the queue to send to the broker together with a
   * callback function that will be invoked when a response is received from
   * the broker for this message.
   *
   * The callback function **must** be thread safe. It will be invoked on an
   * arbitrary thread and hence should not make any assumptions about which
   * thread it gets executed on.
   *
   * The callback function must return `false` to indicate that the function is
   * no longer expecting additional messages and can be released or `true` to
   * indicate that we are expecting additional messages.
   *
   */
  void enqueue(comm::Message&& msg, std::function<bool(comm::Message&&)>&& f);

  /**
   * Enqueue a `raw` message. Use this with exterme caution. This is currently
   * only needed to send back replies which are framed specially. We should fix
   * that in the future to avoid having to expose this internally detail.
   */
  void enqueue_without_framing(comm::Message&& msg);

private:
  Pipe(const Pipe&) = delete;
  void operator=(const Pipe&) = delete;

  std::thread::id OwnerThreadId; //< used in debug builds to assert on thread-unsafe invocations.
  std::string ServiceName;
  std::function<void(comm::Message&&)> Callback0;
  std::future<bool> TaskStatus;

  std::shared_ptr<PInternals> Internals;
};

} // namespace comm::internal
} // namespace comm

#endif
