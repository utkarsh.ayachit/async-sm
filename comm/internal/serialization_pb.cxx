#include "serialization_pb.h"

#include "comm.pb.h"

namespace comm
{
namespace serialization
{
void save(zmq::message_t& message_frame, const google::protobuf::Message& item)
{
  comm::MessageWrapper wrapper;
  wrapper.mutable_content()->PackFrom(item);
  size_t size = wrapper.ByteSizeLong();
  std::unique_ptr<char[]> data(new char[size]);
  wrapper.SerializeToArray(data.get(), static_cast<int>(size));
  message_frame = zmq::message_t(data.get(), size);
}
}
}
