#ifndef comm_utilities_h
#define comm_utilities_h

namespace comm
{
/**
 * @class Filter
 * @brief filter incoming messages
 *
 * It is not uncommon to want to filter out incoming messages on a receiver.
 * This is doing by implementing a `stlab::process` concept.
 * `comm::Filter` is a helper to implement such a process that gets executed
 * whenever a value is passed into to channel with the ability to discard
 * messages for which a callback function returns false.
 *
 * e.g. the following example passes every 10th message down the pipe
 *
 * @code
 * std::atomic_int counter{0};
 * auto hold = client.subscribe("notifications")
 *             | comm::MessageFilter(
 *                  [&counter_input](const std::shared_ptr<const comm::Message>&) {
 *                  return (++counter_input % 10) != 0 ? true : false;
 *                })
 *             | [](std::shared_ptr<const comm::Message>&&) { ...  };
 * hold.set_ready();
 * @endcode
 *
 * `comm::MessageFilter` is simply an alias for `comm::Filter<std::shared_ptr<const
 * comm::Message>`.
 *
 * Use receiver side throttling with caution. In most cases, it may be
 * worthwhile investigating approaches to avoid sending too many (or too
 * frequent) messages on the sending side itself.
 */
template<typename T>
struct Filter
{
  void await(T&& item)
  {
    if (!this->callback(item))
    {
      this->curitem = std::move(item);
      this->curstate = stlab::yield_immediate;
    }
    else
    {
      this->curitem.reset();
    }
  }

  T yield()
  {
    this->curstate = stlab::await_forever;
    return std::move(this->curitem);
  }

  auto state() const { return this->curstate; }

  Filter(std::function<bool(T)> ff)
    : callback(ff){};

private:
  T curitem = T{};
  stlab::process_state_scheduled curstate = stlab::await_forever;
  std::function<bool(T)> callback;
};

/**
 * @class MessageFilter
 * @brief an alias for comm::Filter<std::shared_ptr<const comm::Message>>
 *
 * MessageFilter is simply an alias for `comm::Filter<std::shared_ptr<const
 * comm::Message>`
 */
using MessageFilter = Filter<std::shared_ptr<const comm::Message> >;
}

#endif
