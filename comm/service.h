#ifndef comm_service_h
#define comm_service_h

#include <future>
#include <memory>

#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/future.hpp>

namespace comm
{
/**
 * @class Service
 * @brief component for requesting or performing work.
 *
 * Service represents a component that requests work or does work.
 * It can represent both a client and a server in a client-server framework;
 * there is no distinction between the two.
 *
 * A service connects to the broker (`comm::Broker`) and exchanges messages with
 * other services via the broker. The service API is deliberately bare-bones and
 * provides building blocks to developer richer components.
 *
 * Service name
 * =============
 *
 * Every service has a name. The name is specified in the constructor. This is
 * the name used to register a service with the broker.
 *
 * A service name is expected to be unique, although not enforced.
 * A message intended for a particular service will be dispatched to
 * all services with the same name. There is no way of distinguishing between
 * multiple services with the same name.
 *
 * Names beginning with "_" are reserved.
 *
 * Connecting (and disconnecting) to a Broker
 * ===========================================
 *
 * The service must connect to a broker using `Service::connect` before any of its
 * other API can be used. The method returns a future that indicates the status
 * of whether the connection was successful. All send/receive calls must be
 * delayed until a successful connection has been established.
 *
 * To close a connection, use `Service::close` or simply destroy the Service
 * instance. The destructor of this class also calls `close` to close the
 * connection as part of the clean up.
 *
 * Thread safety
 * ===============
 *
 * Most of the API on `Service` class is thread safe except the ones that are
 * marked explicitly viz. `connect` and `close`. `connect` and `close` must be
 * called on the same thread that the instance was created on. All other API can
 * be invoked on any thread. Furthermore, all APIs that return a `stlab::future`
 * or `stlab::receiver`, by default use `stlab::default_executor` which implies
 * any arbitrary thread from a thread pool may be used for any continuation.
 *
 * Communication Patterns
 * ======================
 *
 * Service API supports two main communication patterns: request-reply, and
 * publish-subscribe.
 *
 * Request-Reply
 * -------------
 *
 * Request-reply pattern allows a service to send a request message to another
 * service and get a response back.
 * To send to request message, use Service::send_request. Returned value is a
 * future that supports continuations to do further processing of the returned
 * result.
 * To send a request without getting a response message back from the target
 * service, use Service::send_message instead.
 *
 * Here are a few snippets how to access results from a SendRequest invocation.
 *
 * @code{cpp}
 * comm::Message req = ...;
 *
 * auto f = service.SendRequest("target-service-name", std::move(req));
 *
 * // Block till the result is available (use sparingly, if at all).
 * comm::Message reply = stlab::blocking_get(f);
 *
 * // Use a continuation to do work.
 * auto hold = f.then(stlab::default_executor,
 *    [](const comm::Message& reply){
 *        // do stuff with reply on arbitrary thread.
 *    });
 *
 * // note, `hold` is a `stlab::future` and must be held as long as a you are
 * // interested in the continuation i.e. if the `hold` gets out of scope, then the
 * // callback function in `then` won't get called. You can use
 * // `stlab::future<T>::detach` to let the continuation proceed even if the future
 * // gets out of scope if absolutely necessary.
 * @endcode
 *
 * While continuations using `then()` are handy, an important question is
 * which thread is the code in the continuation running on? By default, the
 * continuation is executed an arbitrary thread from the internal thread pool.
 * Generally, you want that code to run on the same thread that the request was
 * made i.e the same thread calling the `service.SendRequest`.
 * This can be done by using the `stlab::extensions::loop_executor` as follows.
 *
 * @code{cpp}
 * auto f = service.SendRequest("target-service-name", std::move(req));
 *
 * auto executor = stlab::extensions::loop_executor_type{};
 * auto hold = f.then(executor, [](const comm::Message& reply){ ... });
 *
 * // run the event loop
 * executor.loop();
 * @endcode
 *
 * `stlab::executor::loop_executor_type::loop` will process all events in the
 * current thread.
 *
 * You can combine multiple types of executors too, e.g. to do some heavy data processing in a
 * different thread and then some follow-on work on the main thread, do the
 * following:
 *
 * @code{cpp}
 * auto f = service.SendRequest("target-service-name", std::move(req));
 * auto loop_executor = stlab::extensions::loop_executor_type{};
 * auto hold = f.then(stlab::default_executor,
 *                    [](const comm::Message& reply) {
 *                      // do thread-safe work
 *                      return true;
 *                    }).then(stlab::loop_executor,
 *                    [](bool) {
 *                      // do work in main-loop.
 *                    });
 *
 * loop_executor.loop();
 * @endcode
 *
 * On the receiving end, to respond to such send_request and send_message
 * requests, one can add listener pipelines to receiver on the request channel
 * using `Service::get_request_channel_receiver`.
 *
 * @code{cpp}
 * auto loop_executor = stlab::extensions::loop_executor_type{};
 * auto recv = service.get_request_channel_receiver();
 * auto hold = recv | stlab::executor(loop_executor) & [&](std::shared_ptr<const comm::Message> msg)
 * { comm::Message reply;
 *    // populate reply.
 *    service.send_reply(*msg, std::move(reply));
 *  };
 *
 * // this is important to start receiving messages.
 * recv.set_ready();
 *
 * ...
 *
 * loop_executor.loop();
 * @endcode
 *
 * Request/No-Reply
 * ----------------
 *
 * A variation of the Request-Reply is to make a request but ignore the reply.
 * For such cases, use `Service::send_message` instead of `send_request`. On the
 * receiving end, the same handlers are for `send_request` are invoked. Any calls
 * to `send_reply` in repose, however, are simply ignored.
 *
 * Publish/Subscribe
 * -----------------
 *
 * Pub/Sub provides a communication mechanism where a service can post a message
 * on a named channel and all services that have subscribed to that channel
 * receive that message.
 *
 * To publish a message to a channel, use `Service::publish`.
 *
 * @code{cpp}
 *
 * comm::Message msg = ...
 * service.publish("channel-name", std::move(msg));
 *
 * @endcode
 *
 * To subscribe to a channel and process messages on it, use the
 * `Service::subscribe`. The method returns a
 * `stlab::receiver<std::shared_ptr<const comm::Message> >`
 * instance. One can add process objects to it similar to
 * `Service::get_request_channel_receiver`.
 *
 * @code{cpp}
 * auto receiver = service.subscribe("notifications");
 *
 * auto executor = stlab::extensions::loop_executor_type{};
 * auto hold = receiver |
 *   stlab::executor(executor) & [](std::shared_ptr<const comm::Message> msg) { LOG_S(INFO) << "got
 * notification";
 * };
 *
 * auto hold2 = receiver |
 *   stlab::executor(executor) & [](std::shared_ptr<const comm::Message> msg) { LOG_S(INFO) << "got
 * notification";
 * };
 *
 * receiver.set_ready();
 * @endcode
 *
 * Messages
 * ---------
 *
 * All messages sent and received on a Service are of the type comm::Message
 * which is a collection of comm::Frame instances. A message
 * can have zero or more frames. The message passed to any of the message
 * sending APIs gets padded with a header (empty frame) and a footer (binary
 * representation of `comm::Footer`). These paddings are used internally by the
 * Service and Broker to route and handle the message.
 *
 * Message Order
 * -------------
 *
 * Here are some important aspects to keep in mind about message order.
 * 1. When multiple send_request, subscribe, and publish calls are issued the
 *    corresponding messages will be dispatched to the broker in the same order
 *    that they are issued.
 * 2. The messages will be processed on the target service in the same order as
 *    well.
 */

class Message;

class Service
{
public:
  Service(const std::string& name);
  ~Service();

  /**
   * Returns the name for the service.
   */
  const std::string get_name() const;

  /**
   * Connect to the broker. This returns a future that indicates the status of
   * the connection. The future is not set until the broker has responded and
   * the handshake between the service and the broker is completed (either
   * successfully or in failure). The future indicates the state of the
   * handshake.
   *
   * This method must be called on the same thread on which the Service instance
   * was created.
   *
   * @param brokerurl url for the broker.
   *
   * @returns future that is set to true if and when the connection succeeds
   *          to false otherwise.
   */
  stlab::future<bool> connect(const std::string& brokerurl);

  /**
   * Close this service. This may block till all internal resources are cleaned
   * up.
   *
   * This method must be called on the same thread on which the Service instance
   * was created.
   *
   */
  void close();

  /**
   * Send a message as a request to another service and get a reply. This returns a
   * stlab::future that will hold the response, on success.
   *
   * The future may raise an exceptional condition under following circumstances:
   * 1. the request was sent before a connection is setup or after a connection has been terminated
   *    (comm::InvalidConnection).
   * 2. the connection is closed before the response was received (comm::ConnectionClosed).
   * 3. the receiving service does not handle the request (comm::InvalidRequest).
   *
   * This method is **thread safe** and can be called on any thread.
   *
   * @param service name of the destination service.
   * @param msg message frames to send to the destination service.
   *
   * @returns future that contains the response received from the destination service or
   *          exception (as described earlier).
   */
  stlab::future<Message> send_request(const std::string& service, Message&& msg) const;

  /**
   * Same as `send_request`, however no reply is expected from the destination service.
   * On the receiving end, this is handled exactly as the `send_request`
   * call, except that any `send_reply` invocations in response to this
   * message are quietly ignored.
   *
   * Since this method does not return status, any of the conditions that could have caused
   * the future returned in `send_request` to be set to an exceptional state e.g.
   * connection invalid, will be ignored quietly.
   *
   * This method is **thread safe** and can be called on any thread.
   *
   * @param service name of the destination service.
   * @param msg message frames to send to the destination service.
   */
  void send_message(const std::string& service, Message&& msg) const;

  /**
   * Send a reply for a request. The `replyTo` parameter provides indicates which
   * request this reply is intended for.
   *
   * This method is **thread safe** and can be called on any thread.
   *
   */
  void send_reply(const Message& replyTo, Message&& reply) const;

  /**
   * Subscribe to a channel.
   *
   * This method is **thread safe** and can be called on any thread.
   *
   */
  stlab::receiver<std::shared_ptr<const comm::Message> > subscribe(
    const std::string& channel) const;

  /**
   * Publish a message on a channel. It will be sent to all services subscribed to this channel.
   * If multiple services publish to the same channel, the order in which the messages are received
   * by the subscribing services is not defined.
   *
   * This method is **thread safe** and can be called on any thread.
   *
   */
  bool publish(const std::string& channel, Message&& msg) const;

  /**
   * Returns the main request channel receiver for the service. The request channel
   * receiver is invoked for handling requests and messages sent to this service
   * by other services using `send_request` or `send_message`.
   *
   * This method is **thread safe** and can be called on any thread.
   *
   */
  stlab::receiver<comm::Message>& get_request_channel_receiver() const;

private:
  Service(const Service&) = delete;
  void operator=(const Service&) = delete;

  std::thread::id OwnerThreadId; //< used in debug builds to assert on thread-unsafe invocations.
  class SInternals;
  std::unique_ptr<SInternals> Internals;
};
}

#endif
