#ifndef comm_exceptions_h
#define comm_exceptions_h

#include <stdexcept>

namespace comm
{

class InvalidConnection : public std::runtime_error
{
  using std::runtime_error::runtime_error;
};

class ConnectionClosed : public std::runtime_error
{
  using std::runtime_error::runtime_error;
};

class InvalidRequest : public std::runtime_error
{
  using std::runtime_error::runtime_error;
};

} // namespace comm

#endif
