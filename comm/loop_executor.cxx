#include "loop_executor.h"

namespace comm
{
namespace detail
{
task_queue task_queue::the_queue;
task_queue& task_queue::only_task_queue()
{
  return task_queue::the_queue;
}
}
}
