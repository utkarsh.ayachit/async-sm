#ifndef comm_message_h
#define comm_message_h

#include <memory>
#include <zmq.hpp>

#include "serialization.h"

namespace comm
{

using Frame = zmq::message_t;

/**
 * @class Message
 * @brief A class representing a unit of data blob communicated using the `comm`
 *        layer.
 *
 * `Message` class represents an atomic data block communicated between
 * services (`Service`). It comprises of multiple frames, each of type
 * `Frame`. A `Message` is simply a collection of `Frame`
 * frames with convenience API.
 *
 */
class Message : public std::vector<Frame>
{
  using Superclass = std::vector<Frame>;

public:
  Message() = default;
  ~Message() = default;
  Message(Message&&) = default;
  Message& operator=(Message&&) = default;

  /**
   * Copy-assignment operator is not supported. Use `Clone()` or move-assignment
   * operator instead.
   */
  Message& operator=(const Message&) = delete;
  Message(const Message&) = delete;

  using Superclass::push_back;

  /**
   * Push back all frames from `msg`.
   */
  void push_back(Message&& msg);

  /**
   * Append an empty frame. An empty frame is often used as a delimiter between
   * various parts of the message.
   */
  void empty_back();

  //@{
  /**
   * Extensible API to append arbitrary data to  the message. One can always
   * create a new comm::Frame and append that, however, this provides a
   * convenient mechanism to automate that by simply providing specializations
   * for `comm:::serialization::save(Frame&, const T&)`.
   */
  template<typename T>
  inline void push_back(const T& stuff)
  {
    using namespace comm::serialization;
    Frame msg;
    save(msg, stuff);
    this->push_back(std::move(msg));
  }

  //@{
  /**
   * Append a message to the end by cloning it.
   */
  void clone_back(const Frame& msg);
  void clone_back(const Message& msg);
  //@}

  /**
   * Clones this message and returns the clone.
   */
  Message clone() const { return this->clone(this->begin(), this->end()); }

  /**
   * Clone message frames in the interval `[begin, end)`.
   */
  static Message clone(const_iterator begin, const_iterator end);

  /**
   * Returns iterators for the envelope.
   */
  std::pair<const_iterator, const_iterator> get_envelope() const;
  std::pair<iterator, iterator> get_envelope();

  //@{
  /**
   * Clears data frames i.e. all frames after first empty frame that marks the
   * end of the message envelope and beginning of the data frame.
   */
  void clear_data(bool remove_delimiter = false);
  void clear_delimiter_and_data() { this->clear_data(true); }
  //@}

  //@{
  /**
   * Clears envelope.
   */
  void clear_envelope(bool remove_delimiter = false);
  void clear_envelope_and_delimiter() { this->clear_envelope(true); }
  //@}

  std::string get_debug_string() const;
};
}

#endif
