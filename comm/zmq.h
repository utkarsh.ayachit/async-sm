#ifndef comm_zmq_h
#define comm_zmq_h

#include <zmq.hpp>
#include "message.h"

namespace comm
{

void zmq_initialize(int io_threads = 1);
void zmq_finalize();

/**
 * Returns the ZMQ context for the current process.
 * There's only 1 ZMQ context for the entire process.
 */
zmq::context_t& get_zmq_context();

/**
 * returns a complete message (including all frames)
 */
Message recv(zmq::socket_t& endpoint);

/**
 * send a message. If frames are empty, nothing is sent.
 */
bool send(Message&& frames, zmq::socket_t& endpoint, int flags = 0);

}

#endif
