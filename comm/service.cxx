#include "service.h"

#include "internal/serialization_pb.h"

#include "exceptions.h"
#include "comm.pb.h"
#include "internal/pipe.h"
#include "message.h"

#include "zmq.h"
#include <zmq.hpp>

#include <functional>

#define LOGURU_IMPLEMENTATION 1
#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/default_executor.hpp>
#include <stlab/concurrency/utility.hpp>
namespace comm
{
/**
 * Handler for all unsolicited messaged received by this service. These are
 * typically `send_message` and `send_request` messages sent by other serivices.
 */
static void DefaultCallback(comm::Message&& frames, stlab::sender<comm::Message>& sender)
{
  comm::Footer footer;
  if (frames.size() == 0 || comm::z2p(frames.back(), &footer) == false)
  {
    LOG_S(WARNING) << "ignoring incorrect message received: " << frames.get_debug_string();
    return;
  }

  if (footer.body().Is<comm::GenericRequest>())
  {
    comm::GenericRequest req;
    footer.body().UnpackTo(&req);
    LOG_S(1) << "got request";

    // from the message frames, erase the last frame. the last frame is the this footer
    // which is not part of the public interface and hence no one can interpret it.
    frames.pop_back();

    if (req.skip_response())
    {
      // the request doesn't need a response back. in which case, we clear the envelope from the
      // message so that we don't make any attempt to route the response back to requesting service.
      frames.clear_envelope(); // note: we don't remove the envelope delimiter.
    }
    // dispatch to the main receiver pipeline.
    // whether this will block till this message is processed or return
    // immediately depends on the executor used to create this channel. We're
    // using `stlab::default_executor` which implies that the `sender()` won't
    // block but queue the message to be executed on a thread from the thread
    // pool. One thing to bear in mind is that even though each message may be
    // processed on a different thread, due to the CSP (communicating sequential
    // process) nature of the channel, next message won't be processed until the
    // previous message has been processed thus guaranteeing order.
    sender(std::move(frames));
  }
  else
  {
    LOG_S(WARNING) << "ignoring incorrect message received: " << frames.get_debug_string()
                   << footer.ShortDebugString();
  }
}

static std::function<void(comm::Message&&)> create_callback(stlab::sender<comm::Message>* sender)
{
  return [sender](comm::Message&& msg) { return DefaultCallback(std::move(msg), *sender); };
}

class Service::SInternals
{
public:
  SInternals(const std::string& name)
    : Name(name)
    , RequestChannel{ stlab::channel<comm::Message>(stlab::default_executor) }
    , Pipe(name, create_callback(&this->RequestChannel.first))
    , ConnectionActive(std::make_shared<std::atomic_bool>(false))
  {
  }
  ~SInternals() {}

  std::string Name;
  std::pair<stlab::sender<comm::Message>, stlab::receiver<comm::Message> > RequestChannel;
  internal::Pipe Pipe;
  const std::shared_ptr<std::atomic_bool> ConnectionActive;

  std::mutex MutexForSubscribersMap;
  std::map<std::string, stlab::receiver<std::shared_ptr<const comm::Message> > > SubscribersMap;
};

//-----------------------------------------------------------------------------
Service::Service(const std::string& name)
  : OwnerThreadId(std::this_thread::get_id())
  , Internals(new Service::SInternals(name))
{
  loguru::set_thread_name(name.c_str());
}

//-----------------------------------------------------------------------------
Service::~Service()
{
  CHECK_EQ_F(
    this->OwnerThreadId, std::this_thread::get_id(), "called on different thread than owner!!!");

  *this->Internals->ConnectionActive = false;
}

//-----------------------------------------------------------------------------
const std::string Service::get_name() const
{
  return this->Internals->Name;
}

//-----------------------------------------------------------------------------
stlab::future<bool> Service::connect(const std::string& brokerurl)
{
  auto& internals = (*this->Internals);
  auto active_ptr = internals.ConnectionActive;
  return internals.Pipe.connect(brokerurl).then([active_ptr](bool connected) {
    *active_ptr = connected;
    return connected;
  });
}

//-----------------------------------------------------------------------------
void Service::close()
{
  CHECK_EQ_F(
    this->OwnerThreadId, std::this_thread::get_id(), "called on different thread than owner!!!");

  auto& internals = (*this->Internals);
  *internals.ConnectionActive = false;
  internals.Pipe.close();

  std::lock_guard<std::mutex>(internals.MutexForSubscribersMap);
  internals.SubscribersMap.clear();
}

//-----------------------------------------------------------------------------
stlab::future<Message> Service::send_request(const std::string& service, Message&& frames) const
{
  auto& internals = (*this->Internals);
  if (!*internals.ConnectionActive)
  {
    // no active connection exists. raise exception.
    throw comm::InvalidConnection("");
  }

  LOG_S(INFO) << "sending request to '" << service << "'";
  // add comm::GenericRequest message.
  comm::GenericRequest req;
  req.add_destinations(service);
  req.set_skip_response(false);
  comm::Footer footer;
  footer.mutable_body()->PackFrom(req);

  // add the footer to the message.
  frames.push_back(footer);

  auto f = [](comm::Message&& rframes) {
    LOG_S(1) << "got response: " << rframes.get_debug_string();
    // footer in the frames received is comm::GenericResponse
    // TODO: read footer and raise exceptions, if appropriate.

    // pop footer.
    rframes.pop_back();
    return std::move(rframes);
  };
  auto package = stlab::package<comm::Message(comm::Message &&)>(stlab::default_executor, f);
  auto& task = package.first;
  internals.Pipe.enqueue(std::move(frames), [task](comm::Message&& msg) {
    task(std::move(msg));
    return false; // since we don't expect any more messages.
  });
  return std::move(package.second);
}

//-----------------------------------------------------------------------------
void Service::send_message(const std::string& service, Message&& frames) const
{
  auto& internals = (*this->Internals);
  if (!*internals.ConnectionActive)
  {
    // quietly ignore...no connection.
    return;
  }

  // add comm::GenericRequest message.
  comm::GenericRequest req;
  req.add_destinations(service);
  req.set_skip_response(true); // we don't want response back.

  comm::Footer footer;
  footer.mutable_body()->PackFrom(req);

  // add the footer to the message.
  frames.push_back(footer);

  LOG_S(1) << "sending message";
  internals.Pipe.enqueue(std::move(frames));
}

//-----------------------------------------------------------------------------
void Service::send_reply(const Message& replyTo, Message&& reply) const
{
  // TODO: we want this to be thread safe since this can get called from threads
  // not the main service thread.
  auto& internals = (*this->Internals);
  if (!*internals.ConnectionActive)
  {
    return;
  }

  // copy envelope from `replyTo`.
  auto iterpair = replyTo.get_envelope();
  if (iterpair.first == iterpair.second)
  {
    // attempt to send a reply back for a message, ignore it.
    // note, this is not a error; it's by design.
    return;
  }

  comm::Footer footer;
  comm::GenericResponse response;
  response.set_status(true);
  footer.mutable_body()->PackFrom(response);

  Message message;
  message.empty_back(); // marks beginning of the response message for the broker
  message.push_back(Message::clone(iterpair.first, iterpair.second));
  message.empty_back();
  message.push_back(std::move(reply));
  message.push_back(footer);

  LOG_S(1) << "sending reply";
  internals.Pipe.enqueue_without_framing(std::move(message));
}

//-----------------------------------------------------------------------------
stlab::receiver<std::shared_ptr<const comm::Message> > Service::subscribe(
  const std::string& channelname) const
{
  auto& internals = (*this->Internals);
  if (!internals.ConnectionActive)
  {
    throw comm::InvalidConnection("not connected.");
  }

  std::unique_lock<std::mutex> guard(internals.MutexForSubscribersMap);
  try
  {
    return internals.SubscribersMap.at(channelname);
  }
  catch (std::out_of_range)
  {
  }

  auto channel = stlab::channel<std::shared_ptr<const comm::Message> >(stlab::default_executor);
  channel.second.set_ready();
  internals.SubscribersMap.emplace(std::make_pair(channelname, std::move(channel.second)));
  guard.unlock();

  LOG_S(INFO) << "subscribe to '" << channelname << "'";
  comm::Subscribe sub;
  sub.set_channel(channelname);

  comm::Footer footer;
  footer.mutable_body()->PackFrom(sub);
  Message frames;
  frames.push_back(footer);

  auto sender_ptr = std::make_shared<decltype(channel.first)>(std::move(channel.first));
  internals.Pipe.enqueue(std::move(frames), [sender_ptr](comm::Message&& msg) {
    if (sender_ptr)
    {
      LOG_S(1) << "got published data: " << msg.get_debug_string();
      assert(msg.size() > 0);
      // remove footer added by matching Service::publish on the sending
      // service.
      msg.pop_back();

      (*sender_ptr.get())(std::make_shared<comm::Message>(std::move(msg)));
    }
    return true; // more messages expected.
  });
  guard.lock();
  return internals.SubscribersMap.at(channelname);
}

//-----------------------------------------------------------------------------
bool Service::publish(const std::string& channelname, Message&& frames) const
{
  auto& internals = (*this->Internals);
  if (!internals.ConnectionActive)
  {
    throw comm::InvalidConnection("not connected.");
  }

  comm::Publish pub;
  pub.set_channel(channelname);

  comm::Footer footer;
  footer.mutable_body()->PackFrom(pub);

  frames.push_back(footer);

  LOG_S(6) << "publish to '" << channelname << "'";
  internals.Pipe.enqueue(std::move(frames));
  return true;
}

//-----------------------------------------------------------------------------
stlab::receiver<comm::Message>& Service::get_request_channel_receiver() const
{
  auto& internals = *(this->Internals);
  return internals.RequestChannel.second;
}

} // namespace comm
