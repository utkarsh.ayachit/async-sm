#include <comm/broker.h>
#include <comm/loop_executor.h>
#include <comm/message.h>
#include <comm/service.h>
#include <comm/utilities.h>
#include <comm/zmq.h>

#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/default_executor.hpp>
#include <stlab/concurrency/utility.hpp>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <chrono>
#include <future>
#include <memory>
#include <thread>

/**
 * This tests ability to throttle messages.
 */

int main(int argc, char** argv)
{
  // Turn off individual parts of the preamble
  loguru::g_preamble_date = false; // The date field
  loguru::g_preamble_time = false; // The time of the current day
  loguru::init(argc, argv);
  // loguru::add_file("/tmp/everything.log", loguru::Truncate, loguru::Verbosity_MAX);

  //---------------------------------------------------------------------------
  // BROKER
  //---------------------------------------------------------------------------
  auto retvalB = std::async(std::launch::async, []() {
    comm::Broker broker;
    return broker.exec("tcp://*:11111") == EXIT_SUCCESS ? true : false;
  });
  using namespace std::chrono_literals;
  std::this_thread::sleep_for(10ms);

  //---------------------------------------------------------------------------
  // SERVER
  //---------------------------------------------------------------------------
  auto retvalDS = std::async(std::launch::async, []() {
    comm::Service ds("server");
    stlab::blocking_get(ds.connect("tcp://localhost:11111"));
    auto executor = comm::loop_executor_type{};
    int state = 0;
    auto& recv = ds.get_request_channel_receiver();
    auto hold =
      recv | stlab::executor(executor) & [&ds, &state, executor](const comm::Message& msg) {
        LOG_S(INFO) << "got req (will reply)  " << msg.get_debug_string();
        comm::Message reply;
        ds.send_reply(msg, std::move(reply));
        state++;
        if (state == 1)
        {
          try
          {
            // start pumping out messages at a high speed for 10 seconds.
            int counter = 0;
            auto start_time = std::chrono::steady_clock::now();
            while (std::chrono::duration_cast<std::chrono::seconds>(
                     std::chrono::steady_clock::now() - start_time)
                     .count() < 10)
            {
              ds.publish("notifications", comm::Message());
              // anything more that 1K messages per second is problematic
              // and not supported currently.
              ++counter;
            }
            LOG_S(INFO) << "sent notifications at " << counter / (10 * 1000.0) << " KHz";
          }
          catch (std::exception& e)
          {
            LOG_S(INFO) << "EXCEPTION!!! " << e.what();
          }
          LOG_S(INFO) << "make-exit.";
          executor.make_loop_exit();
          LOG_S(INFO) << "exit.";
        }
      };
    recv.set_ready();
    executor.loop();
    // send empty message to client to indicate we're done here.
    try
    {
      LOG_S(INFO) << "send goodbye";
      stlab::blocking_get(ds.send_request("client", comm::Message()));
    }
    catch (std::exception& e)
    {
      LOG_S(INFO) << "EXCEPTION!!! " << e.what();
    }
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(10ms);
  });

  //---------------------------------------------------------------------------
  // CLIENT
  //---------------------------------------------------------------------------
  comm::Service client("client");
  stlab::blocking_get(client.connect("tcp://localhost:11111"));

  auto executor = comm::loop_executor_type{};
  std::atomic_int counter_input{ 0 };
  std::atomic_int counter_output{ 0 };

  auto hold = client.subscribe("notifications") |
    comm::MessageFilter([&counter_input](const std::shared_ptr<const comm::Message>&) {
      return (++counter_input % 100000) != 0 ? true : false;
    }) |
    [&counter_input, &counter_output](std::shared_ptr<const comm::Message>&&) {
      LOG_S(INFO) << "here:" << counter_input;
      ++counter_output;
    };
  hold.set_ready();

  // add listerner to exit when we get a message from server.
  auto hold2 = client.get_request_channel_receiver() |
    stlab::executor(executor) & [executor, &client](comm::Message&& msg) {
      LOG_S(INFO) << "server done: " << msg.get_debug_string();
      client.send_reply(msg, comm::Message());

      executor.make_loop_exit();
    };
  client.get_request_channel_receiver().set_ready();

  client.send_message("server", comm::Message());
  executor.loop();
  LOG_S(INFO) << "messages in: " << counter_input << " out: " << counter_output;
  return EXIT_SUCCESS;
}
