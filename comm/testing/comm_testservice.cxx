#include <comm/broker.h>
#include <comm/loop_executor.h>
#include <comm/message.h>
#include <comm/service.h>
#include <comm/zmq.h>
#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/utility.hpp>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <chrono>
#include <future>
#include <memory>
#include <thread>

/**
 * This test connects a service to a broker and sends a message to get a reply
 * back and exit gracefully.
 */
int main(int argc, char** argv)
{
  // Turn off individual parts of the preamble
  loguru::g_preamble_date = false; // The date field
  loguru::g_preamble_time = false; // The time of the current day
  loguru::init(argc, argv);

  //---------------------------------------------------------------------------
  // BROKER
  //---------------------------------------------------------------------------
  auto retvalB = std::async(std::launch::async, []() {
    comm::Broker broker;
    return broker.exec("inproc://comm_testservice") == EXIT_SUCCESS ? true : false;
  });
  using namespace std::chrono_literals;
  std::this_thread::sleep_for(10ms);

  //---------------------------------------------------------------------------
  // SERVER
  //---------------------------------------------------------------------------
  auto retvalS = std::async(std::launch::async, []() {
    comm::Service service("server");
    auto f = service.connect("inproc://comm_testservice");
    if (!stlab::blocking_get(f))
    {
      LOG_S(ERROR) << "failed to connect to broker!";
      return false;
    }

    auto executor = comm::loop_executor_type{};
    auto& rcvr = service.get_request_channel_receiver();
    auto hold =
      rcvr | stlab::executor(executor) & [&service, executor](comm::Message&& req) -> void {
      LOG_S(INFO) << "got request " << req.get_debug_string();
      comm::Message reply;
      reply.emplace_back(zmq::message_t("world", strlen("world") + 1));
      service.send_reply(req, std::move(reply));
      if (req.size() > 1)
      {
        executor.make_loop_exit();
      }
    };
    rcvr.set_ready();
    executor.loop();
    LOG_S(INFO) << "done";
    return true;
  });

  //---------------------------------------------------------------------------
  // CLIENT
  //---------------------------------------------------------------------------
  comm::Service service("client");
  if (!stlab::blocking_get(service.connect("inproc://comm_testservice")))
  {
    LOG_S(ERROR) << "failed to connect to broker!";
    return EXIT_FAILURE;
  }

  auto r = service.subscribe("test");
  auto executor = comm::loop_executor_type{};

  service.send_message("server", comm::Message());

  comm::Message msg;
  msg.emplace_back(zmq::message_t("hello", strlen("hello") + 1));
  auto f = service.send_request("server", std::move(msg))
             .then(executor, [executor](const comm::Message& reply) {
               LOG_S(INFO) << "got reply " << reply.get_debug_string();
               executor.make_loop_exit();
             });
  executor.loop();
  service.close();
  LOG_S(INFO) << "done";
  return (retvalB.get() && retvalS.get()) ? EXIT_SUCCESS : EXIT_FAILURE;
}
