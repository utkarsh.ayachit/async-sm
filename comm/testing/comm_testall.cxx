#include <comm/broker.h>
#include <comm/loop_executor.h>
#include <comm/message.h>
#include <comm/service.h>
#include <comm/zmq.h>
#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/utility.hpp>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <chrono>
#include <future>
#include <memory>
#include <thread>

int main(int argc, char* argv[])
{
  // Turn off individual parts of the preamble
  loguru::g_preamble_date = false; // The date field
  loguru::g_preamble_time = false; // The time of the current day
  loguru::init(argc, argv);

  //---------------------------------------------------------------------------
  // BROKER
  //---------------------------------------------------------------------------
  auto retvalB = std::async(std::launch::async, []() {
    comm::Broker broker;
    return broker.exec("tcp://*:11111") == EXIT_SUCCESS ? true : false;
  });
  using namespace std::chrono_literals;
  std::this_thread::sleep_for(10ms);

  //---------------------------------------------------------------------------
  // DATA-SERVER
  //---------------------------------------------------------------------------
  auto retvalDS = std::async(std::launch::async, []() {
    comm::Service ds("data-server");
    stlab::blocking_get(ds.connect("tcp://localhost:11111"));

    auto executor = comm::loop_executor_type{};
    int awaiting_messages = 1;
    auto& recv = ds.get_request_channel_receiver();
    auto hold = recv |
      stlab::executor(executor) & [&ds, &awaiting_messages, executor](const comm::Message& msg) {
        LOG_S(INFO) << "got req (will reply)  " << msg.get_debug_string();
        comm::Message reply;
        ds.send_reply(msg, std::move(reply));

        comm::Message note;
        ds.publish("notifications", std::move(note));

        if (--awaiting_messages == 0)
        {
          executor.make_loop_exit();
        }
      };
    recv.set_ready();
    executor.loop();
  });

  //---------------------------------------------------------------------------
  // CLIENT
  //---------------------------------------------------------------------------
  comm::Service client("client");
  stlab::blocking_get(client.connect("tcp://localhost:11111"));
  auto receiver = client.subscribe("notifications");

  int awaiting_notifications = 2;
  auto executor = comm::loop_executor_type{};
  auto hold = receiver |
    stlab::executor(executor) &
      [&awaiting_notifications, executor](std::shared_ptr<const comm::Message> msg) {
        LOG_S(INFO) << "got notification @1: " << msg->get_debug_string();
        if (--awaiting_notifications == 0)
        {
          executor.make_loop_exit();
        }
      };
  auto hold2 = receiver |
    stlab::executor(executor) &
      [&awaiting_notifications, executor](std::shared_ptr<const comm::Message> msg) {
        LOG_S(INFO) << "got notification @2: " << msg->get_debug_string();
        if (--awaiting_notifications == 0)
        {
          executor.make_loop_exit();
        }
      };
  receiver.set_ready();
  comm::Message frames;
  auto hold3 = client.send_request("data-server", std::move(frames))
                 .then(executor, [](const comm::Message& msg) {
                   LOG_S(INFO) << "got reply for `send_request` " << msg.size();
                 });
  executor.loop();
  client.close();
  LOG_S(INFO) << "done";
  return EXIT_SUCCESS;
}
