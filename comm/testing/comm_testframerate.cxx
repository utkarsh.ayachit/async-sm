#include <comm/broker.h>
#include <comm/loop_executor.h>
#include <comm/message.h>
#include <comm/service.h>
#include <comm/utilities.h>
#include <comm/zmq.h>

#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/default_executor.hpp>
#include <stlab/concurrency/utility.hpp>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <chrono>
#include <future>
#include <memory>
#include <thread>

/**
 * This tests ability to throttle messages.
 */

int main(int argc, char** argv)
{
  // Turn off individual parts of the preamble
  loguru::g_preamble_date = false; // The date field
  loguru::g_preamble_time = false; // The time of the current day
  loguru::init(argc, argv);
  // loguru::add_file("/tmp/everything.log", loguru::Truncate, loguru::Verbosity_MAX);
  //---------------------------------------------------------------------------
  // BROKER
  //---------------------------------------------------------------------------
  auto retvalB = std::async(std::launch::async, []() {
    comm::Broker broker;
    return broker.exec("tcp://*:11111") == EXIT_SUCCESS ? true : false;
  });
  using namespace std::chrono_literals;
  std::this_thread::sleep_for(10ms);

  //---------------------------------------------------------------------------
  // SERVER
  //---------------------------------------------------------------------------
  auto retvalDS = std::async(std::launch::async, []() {
    comm::Service ds("server");
    stlab::blocking_get(ds.connect("tcp://localhost:11111"));
    auto executor = comm::loop_executor_type{};
    std::atomic_int state{ 0 };

    auto& recv = ds.get_request_channel_receiver();
    auto hold = recv | [&ds, &state, executor](comm::Message&& msg) {
      LOG_S(INFO) << "got req (will reply)  " << msg.get_debug_string();
      comm::Message reply;
      ds.send_reply(msg, std::move(reply));
      state++;
    };
    recv.set_ready();
    executor.loop();
    // send empty message to client to indicate we're done here.
    try
    {
      LOG_S(INFO) << "send goodbye";
      ds.send_message("client", comm::Message());
    }
    catch (std::exception& e)
    {
      LOG_S(INFO) << "EXCEPTION!!! " << e.what();
    }
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(10ms);
  });

  using namespace std::chrono_literals;
  std::this_thread::sleep_for(10ms);

  //---------------------------------------------------------------------------
  // CLIENT
  //---------------------------------------------------------------------------
  comm::Service client("client");
  stlab::blocking_get(client.connect("tcp://localhost:11111"));

  auto executor = comm::loop_executor_type{};

  // send a request to the server to confirm it's ready for us.
  stlab::blocking_get(client.send_request("server", comm::Message()));

  // executor.loop();
  return EXIT_SUCCESS;
}
