#include <comm/broker.h>
#include <comm/internal/pipe.h>
#include <comm/zmq.h>
#include <stlab/concurrency/utility.hpp>

#define LOGURU_WITH_STREAMS 1
#define LOGURU_IMPLEMENTATION 1
#include <thirdparty/loguru/loguru.hpp>

#include <chrono>
#include <future>
#include <thread>

int main(int argc, char** argv)
{
  // Turn off individual parts of the preamble
  loguru::g_preamble_date = false; // The date field
  loguru::g_preamble_time = false; // The time of the current day
  loguru::init(argc, argv);

  //---------------------------------------------------------------------------
  // BROKER
  //---------------------------------------------------------------------------
  auto retvalB = std::async(std::launch::async, []() {
    comm::Broker broker;
    return broker.exec("inproc://comm_pipe_test");
  });
  using namespace std::chrono_literals;
  std::this_thread::sleep_for(10ms);

  comm::internal::Pipe pipe("comm_pipe_test", [](comm::Message&&) { return true; });
  auto f = pipe.connect("inproc://comm_pipe_test");
  LOG_S(INFO) << "pipe launch status: " << stlab::blocking_get(f);
  return stlab::blocking_get(f) ? EXIT_SUCCESS : EXIT_FAILURE;
}
