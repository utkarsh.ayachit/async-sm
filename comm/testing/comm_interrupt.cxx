#include <comm/broker.h>
#include <comm/loop_executor.h>
#include <comm/message.h>
#include <comm/service.h>
#include <comm/zmq.h>
#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/default_executor.hpp>
#include <stlab/concurrency/utility.hpp>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <atomic>
#include <chrono>
#include <future>
#include <memory>
#include <thread>

namespace
{

comm::Message s_message(const std::string& command)
{
  comm::Message msg;
  msg.emplace_back(zmq::message_t(command.c_str(), command.size() + 1));
  return msg;
}

comm::Message c_message(const std::string& command, std::int32_t id)
{
  comm::Message msg;
  msg.emplace_back(zmq::message_t(command.c_str(), command.size() + 1));
  msg.emplace_back(zmq::message_t(&id, sizeof(id)));
  return msg;
}

std::pair<std::string, std::int32_t> c_message(const comm::Message& msg0)
{
  // there are better ways of doing this, I am being lazy.
  comm::Message msg;
  msg.clone_back(msg0);
  msg.clear_envelope_and_delimiter();

  CHECK_EQ_F(msg.size(), size_t(2), "bad size mismatch!");
  std::string s(reinterpret_cast<const char*>(msg[0].data()));
  std::int32_t id = *reinterpret_cast<const std::int32_t*>(msg[1].data());
  return std::make_pair(s, id);
}

class vtkObject
{
  std::atomic<uint64_t> AbortTag{ 0 };
  std::atomic<uint64_t> ExecuteTag{ 0 };

public:
  void Abort(std::uint64_t id) { this->AbortTag = id; }

  void Execute(std::uint64_t id)
  {
    if (this->AbortTag > id)
    {
      LOG_S(INFO) << "skipping execute since abort request already in queue.";
      return;
    }

    for (int cc = 0; cc < 100; ++cc)
    {
      using namespace std::chrono_literals;
      std::this_thread::sleep_for(10ms);
      if (this->AbortTag > id)
      {
        LOG_S(WARNING) << "aborted execution!";
        break;
      }
    }
    LOG_S(INFO) << "done execution";
  }
};

class SIManager
{
  std::shared_ptr<comm::Service> Service;
  std::thread::id OwnerThreadId; //< used in debug builds to assert on thread-unsafe invocations.

  std::mutex ObjectMapMutex;
  std::map<std::int32_t, std::shared_ptr<vtkObject> > ObjectMap;

public:
  SIManager(std::shared_ptr<comm::Service> service)
    : Service(service)
    , OwnerThreadId(std::this_thread::get_id())
  {
  }
  ~SIManager() {}

  // NOTE: Preprocess can be called on any thread and hence must be thread
  // safe.
  //
  // returns -1 to indicate done.
  int Preprocess(std::uint64_t message_index, const comm::Message& msg)
  {
    std::string cmd;
    std::int32_t id;
    std::tie(cmd, id) = c_message(msg);
    LOG_S(INFO) << "Pre-process: " << cmd << " " << id;

    if (cmd == "CREATE")
    {
      std::lock_guard<std::mutex>(this->ObjectMapMutex);
      this->ObjectMap[id] = std::make_shared<vtkObject>();
      this->Service->send_reply(msg, comm::Message());
      return 0;
    }
    else if (cmd == "MODIFY")
    {
      std::lock_guard<std::mutex>(this->ObjectMapMutex);
      CHECK_F(this->ObjectMap.find(id) != this->ObjectMap.end(), "invalid object id '%d'", id);
      this->ObjectMap[id]->Abort(message_index);
      return 0;
    }
    else if (cmd == "DOWORK")
    {
      std::lock_guard<std::mutex>(this->ObjectMapMutex);
      CHECK_F(this->ObjectMap.find(id) != this->ObjectMap.end(), "invalid object id '%d'", id);
      return 0;
    }
    else if (cmd == "EXIT")
    {
      this->Service->send_reply(msg, comm::Message());
      return -1;
    }
    throw std::runtime_error("invalid command!");
  }

  // NOTE: `Process` will assume that it is going to be called on the owner
  // thread.
  void Process(std::uint64_t message_index, const comm::Message& msg)
  {
    CHECK_EQ_F(this->OwnerThreadId,
      std::this_thread::get_id(),
      "`Process` must be called on the same thread as the owner thread.");
    std::string cmd;
    std::int32_t id;
    std::tie(cmd, id) = c_message(msg);
    LOG_S(INFO) << "Process: " << cmd << " " << id;
    if (cmd == "MODIFY")
    {
      LOG_S(INFO) << "update params on VTK object " << id;
    }
    else if (cmd == "DOWORK")
    {
      std::shared_ptr<vtkObject> obj;
      {
        std::lock_guard<std::mutex>(this->ObjectMapMutex);
        obj = this->ObjectMap[id];
      }
      if (obj)
      {
        obj->Execute(message_index);
      }
    }
  }
};
}

int main(int argc, char* argv[])
{
  // Turn off individual parts of the preamble
  loguru::g_preamble_date = false; // The date field
  loguru::g_preamble_time = false; // The time of the current day
  loguru::init(argc, argv);

  //---------------------------------------------------------------------------
  // BROKER
  //---------------------------------------------------------------------------
  auto retvalB = std::async(std::launch::async, []() {
    comm::Broker broker;
    return broker.exec("tcp://*:11111") == EXIT_SUCCESS ? true : false;
  });
  using namespace std::chrono_literals;
  std::this_thread::sleep_for(10ms);

  //---------------------------------------------------------------------------
  // DataServer
  //---------------------------------------------------------------------------
  auto retvalDS = std::async(std::launch::async, []() {
    auto ds = std::make_shared<comm::Service>("data-server");
    stlab::blocking_get(ds->connect("tcp://localhost:11111"));
    auto manager = std::make_shared<SIManager>(ds);

    auto executor = comm::loop_executor_type{};
    auto& recv = ds->get_request_channel_receiver();
    std::atomic<std::uint64_t> message_id{ 0 };

    auto hold = recv |
      stlab::executor(stlab::default_executor) &
        [manager, executor, &message_id](comm::Message&& msg) {
          auto id = message_id++;
          if (manager->Preprocess(id, msg) == -1)
          {
            executor.make_loop_exit();
          }
          return std::make_pair(id, std::move(msg));
        } |
      stlab::executor(executor) &
        [manager](std::pair<std::uint64_t, comm::Message>&& msg) {
          // assert(this_thread == ds_thread)
          manager->Process(msg.first, msg.second);
        } &
        stlab::buffer_size{ 100 };
    recv.set_ready();
    executor.loop();
    return true;
  });

  auto client = std::make_shared<comm::Service>("client");
  stlab::blocking_get(client->connect("tcp://localhost:11111"));
  client->send_message("data-server", c_message("CREATE", 1));
  client->send_message("data-server", c_message("MODIFY", 1));
  client->send_message("data-server", c_message("MODIFY", 1));
  client->send_message("data-server", c_message("CREATE", 2));
  client->send_message("data-server", c_message("DOWORK", 1));
  client->send_message("data-server", c_message("MODIFY", 1));
  client->send_message("data-server", c_message("DOWORK", 1));
  client->send_message("data-server", c_message("MODIFY", 2));
  std::this_thread::sleep_for(1s);

  client->send_message("data-server", c_message("EXIT", 0));

  return retvalDS.get() ? EXIT_SUCCESS : EXIT_FAILURE;
}
