#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/default_executor.hpp>
#include <stlab/concurrency/serial_queue.hpp>
#include <stlab/concurrency/utility.hpp>

#include <chrono>
#include <future>
#include <iostream>
#include <memory>
#include <thread>

int main(int, char* [])
{
  auto c = stlab::channel<int>(stlab::default_executor);
  auto& sender = c.first;
  auto& receiver = c.second;

  auto hold = receiver |
    /** STEP 1 **/
    [](const int& val) {
      std::cout << "step 1: {" << val << "}" << std::endl;
      return val;
    } |

    /** STEP 2 **/
    [](int val) {
      std::cout << " step 2: start {" << val << "}" << std::endl;
      using namespace std::chrono_literals;
      std::this_thread::sleep_for(1s);
      std::cout << " step 2: end {" << val << "}" << std::endl;
    };
  receiver.set_ready();

  std::cout << "start sending" << std::endl;
  for (int cc = 0; cc < 5; cc++)
  {
    sender(cc);
  }
  std::cout << "done sending" << std::endl;

  using namespace std::chrono_literals;
  std::this_thread::sleep_for(10s);
  std::cout << "done" << std::endl;
  return EXIT_SUCCESS;
}
