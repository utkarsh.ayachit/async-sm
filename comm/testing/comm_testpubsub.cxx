#include <comm/broker.h>
#include <comm/loop_executor.h>
#include <comm/message.h>
#include <comm/service.h>
#include <comm/zmq.h>
#include <stlab/concurrency/channel.hpp>
#include <stlab/concurrency/utility.hpp>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

#include <chrono>
#include <future>
#include <memory>
#include <thread>

/**
 * This test tests publish-subscribe.
 */
int main(int argc, char** argv)
{
  // Turn off individual parts of the preamble
  loguru::g_preamble_date = false; // The date field
  loguru::g_preamble_time = false; // The time of the current day
  loguru::init(argc, argv);

  //---------------------------------------------------------------------------
  // BROKER
  //---------------------------------------------------------------------------
  auto retvalB = std::async(std::launch::async, []() {
    comm::Broker broker;
    return broker.exec("inproc://comm_testpubsub") == EXIT_SUCCESS ? true : false;
  });
  using namespace std::chrono_literals;
  std::this_thread::sleep_for(10ms);

  //---------------------------------------------------------------------------
  // Create a service to subscribe to a channel.
  //---------------------------------------------------------------------------
  auto retvalS1 = std::async(std::launch::async, []() {
    comm::Service service("listener1");
    if (!stlab::blocking_get(service.connect("inproc://comm_testpubsub")))
    {
      LOG_S(ERROR) << "failed to connect to broker.";
      return false;
    }

    int counter = 0;
    auto executor = comm::loop_executor_type{};
    auto rcvr = service.subscribe("blabber");
    auto hold = rcvr |
      stlab::executor(executor) &
        [&counter, executor](std::shared_ptr<const comm::Message> message) -> void {
      LOG_S(INFO) << "got published message: " << message->get_debug_string();
      CHECK_EQ_F(message->size(), 0u, "expecting 0 size messages.");
      // exit after 5 messages.
      counter++;
      if (counter == 5)
      {
        executor.make_loop_exit();
      }
    };
    rcvr.set_ready();
    executor.loop();
    LOG_S(INFO) << "done";
    return true;
  });
  using namespace std::chrono_literals;
  std::this_thread::sleep_for(10ms);

  //---------------------------------------------------------------------------
  // Create a service to publish messages.
  //---------------------------------------------------------------------------
  comm::Service service("talker");
  if (!stlab::blocking_get(service.connect("inproc://comm_testpubsub")))
  {
    LOG_S(ERROR) << "failed to connect to broker.";
    return EXIT_FAILURE;
  }
  service.publish("blabber", comm::Message());
  service.publish("blabber", comm::Message());
  service.publish("blabber", comm::Message());
  service.publish("blabber", comm::Message());
  service.publish("blabber", comm::Message());
  service.close();
  LOG_S(INFO) << "done";
  return (retvalB.get() && retvalS1.get()) ? EXIT_SUCCESS : EXIT_FAILURE;
}
