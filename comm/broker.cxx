#include "broker.h"
#include "comm.pb.h"
#include "internal/serialization_pb.h"

#include "message.h"
#include "zmq.h"

#include <iterator>
#include <zmq.hpp>


#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

namespace comm
{

class Broker::BInternals
{
  std::map<std::string, std::vector<Message> > ServiceEnvelopes;
  std::map<std::string, std::vector<Message> > SubscriptionEnvelopes;
  bool WaitingForTermination = false;

public:
  comm::GreetingResponse do_greeting_request(const comm::GreetingRequest& req,
    const Message& incoming)
  {
    // clone all frames except the footer and the separator.
    Message frames_clone =
      incoming.clone(incoming.begin(), std::next(incoming.begin(), incoming.size() - 1));
    assert(frames_clone.back().size() == 0);
    frames_clone.pop_back();

    this->ServiceEnvelopes[req.service_name()].push_back(std::move(frames_clone));
    LOG_S(INFO) << "registered service for '" << req.service_name() << "'";
    comm::GreetingResponse res;
    res.set_status(true);
    return res;
  }

  bool do_generic_request(const comm::GenericRequest& req,
    const Message& incoming,
    zmq::socket_t& endpoint)
  {
    bool handled = false;
    for (const auto& dest : req.destinations())
    {
      auto seiter = this->ServiceEnvelopes.find(dest);
      if (seiter == this->ServiceEnvelopes.end() || seiter->second.size() == 0)
      {
        LOG_S(INFO) << "no service found for '" << dest << "'";
      }
      else
      {
        LOG_S(1) << "forward to " << dest << " (count=" << seiter->second.size() << ")";
        for (const auto& senv : seiter->second)
        {
          auto clone = senv.clone();
          clone.clone_back(incoming);
          comm::send(std::move(clone), endpoint);
          handled = true;
        }
      }
    }
    return handled;
  }

  void do_subscribe(const comm::Subscribe& req, Message&& msg, zmq::socket_t& endpoint)
  {
    const auto channel = req.channel();
    LOG_S(INFO) << "registered subscription for '" << channel << "'";
    msg.clear_delimiter_and_data();
    if (!this->WaitingForTermination)
    {
      this->SubscriptionEnvelopes[channel].push_back(std::move(msg));
    }
    else
    {
      auto env_pair = msg.get_envelope();
      Message data = Message::clone(env_pair.first, env_pair.second);
      comm::Goodbye treq;
      comm::Footer footer;
      footer.mutable_body()->PackFrom(treq);
      data.empty_back();
      data.push_back(footer);
      comm::send(std::move(data), endpoint);
    }
  }

  void do_publish(const comm::Publish& pub, Message&& msg, zmq::socket_t& endpoint) const
  {
    const auto channel = pub.channel();
    if (this->SubscriptionEnvelopes.find(channel) == this->SubscriptionEnvelopes.end())
    {
      LOG_S(1) << "No subscriptions for " << channel << ". Ignoring.";
      return;
    }

    LOG_S(1) << "broadcasting message for '" << channel << "' " << msg.get_debug_string();
    msg.clear_envelope_and_delimiter();

    for (const auto& senv : this->SubscriptionEnvelopes.at(channel))
    {
      auto env_pair = senv.get_envelope();

      Message data = Message::clone(env_pair.first, env_pair.second);
      data.empty_back();
      data.clone_back(msg);
      comm::send(std::move(data), endpoint);
    }
  }

  bool do_goodbye(const comm::Goodbye&, Message&& incoming, zmq::socket_t& endpoint)
  {
    // remove the service sending us the goodbye. it is no longer capable of
    // listening to our messages.

    // purge the service from our talk-back data structures.
    // since broker is a ZMQ_ROUTER, the first message is the connection
    // identity

    Message data;
    data.clone_back(incoming);
    data.clear_envelope_and_delimiter();

    const auto& identity = incoming.front();
    for (auto& sepairs : this->SubscriptionEnvelopes)
    {
      for (auto iter = sepairs.second.begin(); iter != sepairs.second.end();)
      {
        if (iter->front() == identity)
        {
          iter = sepairs.second.erase(iter);
          LOG_S(1) << "dropping subscription for " << sepairs.first;
        }
        else
        {
          ++iter;
        }
      }
    }

    int other_service_count = 0;
    // purge the service and let other services know that this service is going
    // away.
    for (auto& sepairs : this->ServiceEnvelopes)
    {
      for (auto iter = sepairs.second.begin(); iter != sepairs.second.end();)
      {
        if (iter->front() == identity)
        {
          iter = sepairs.second.erase(iter);
        }
        else
        {
          auto env_pair = iter->get_envelope();
          Message clone = Message::clone(env_pair.first, env_pair.second);
          clone.empty_back();
          clone.clone_back(data);
          comm::send(std::move(clone), endpoint);
          ++other_service_count;
          ++iter;
        }
      }
    }

    return other_service_count == 0;
  }
};

//-----------------------------------------------------------------------------
Broker::Broker()
  : Internals(new Broker::BInternals())
{
}

//-----------------------------------------------------------------------------
Broker::~Broker()
{
}

//-----------------------------------------------------------------------------
int Broker::exec(const std::string& url)
{
  auto& internals = (*this->Internals);

  loguru::set_thread_name("broker");

  zmq::socket_t endpoint{ comm::get_zmq_context(), ZMQ_ROUTER };
  endpoint.setsockopt(ZMQ_ROUTER_MANDATORY, 1);
  endpoint.setsockopt(ZMQ_LINGER, 0);
  endpoint.setsockopt(ZMQ_IMMEDIATE, 1);
  endpoint.setsockopt(ZMQ_SNDHWM, 500000);
  endpoint.bind(url);
  LOG_S(INFO) << "waiting on '" << url << "'";

  comm::Footer footer;
  while (true)
  {
    auto message_frames = comm::recv(endpoint);
    // parse footer and then decide the action.
    if (!comm::z2p(message_frames.back(), &footer))
    {
      LOG_S(INFO) << "message has no valid footer!!! Ignoring. "
                  << message_frames.get_debug_string();
      continue;
    }

    if (footer.body().Is<comm::GreetingRequest>())
    {
      LOG_S(1) << "got comm::GreetingRequest";
      comm::GreetingRequest greq;
      footer.body().UnpackTo(&greq);

      auto response = internals.do_greeting_request(greq, message_frames);
      footer.mutable_body()->PackFrom(response);

      message_frames.clear_data();
      message_frames.push_back(footer);
      comm::send(std::move(message_frames), endpoint);
    }
    else if (footer.body().Is<comm::GenericRequest>())
    {
      LOG_S(1) << "got comm::GenericRequest";
      comm::GenericRequest greq;
      footer.body().UnpackTo(&greq);
      if (!internals.do_generic_request(greq, message_frames, endpoint))
      {
        // reply back failure.
        comm::GenericResponse res;
        res.set_source("broker");
        res.set_status(false);
        footer.mutable_body()->PackFrom(res);

        message_frames.clear_data();
        message_frames.push_back(footer);
        comm::send(std::move(message_frames), endpoint);
      }
    }
    else if (footer.body().Is<comm::GenericResponse>())
    {
      LOG_S(1) << "got comm::GenericResponse " << message_frames.get_debug_string();

      // remove enveloppe (including delimiter) and send the message
      message_frames.clear_envelope_and_delimiter();
      comm::send(std::move(message_frames), endpoint);
    }
    else if (footer.body().Is<comm::Subscribe>())
    {
      LOG_S(1) << "got comm::Subscribe";
      comm::Subscribe sreq;
      footer.body().UnpackTo(&sreq);
      internals.do_subscribe(sreq, std::move(message_frames), endpoint);
    }
    else if (footer.body().Is<comm::Publish>())
    {
      LOG_S(1) << "got comm::Publish";
      comm::Publish spub;
      footer.body().UnpackTo(&spub);
      internals.do_publish(spub, std::move(message_frames), endpoint);
    }
    else if (footer.body().Is<comm::Goodbye>())
    {
      comm::Goodbye gbye;
      footer.body().UnpackTo(&gbye);
      LOG_S(1) << "got comm::Goodbye " << gbye.ShortDebugString();
      if (internals.do_goodbye(gbye, std::move(message_frames), endpoint))
      {
        break;
      }
    }
    else
    {
      LOG_S(INFO) << "unhandled message: " << footer.ShortDebugString();
    }
  }

  return EXIT_SUCCESS;
}
}
