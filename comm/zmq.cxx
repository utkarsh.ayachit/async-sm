#include "zmq.h"

#include <memory>
#include <mutex>

#include "comm.pb.h"
#include <zmq.hpp>

#define LOGURU_WITH_STREAMS 1
#include <thirdparty/loguru/loguru.hpp>

namespace comm
{

namespace detail
{
static std::mutex Mutex;
static zmq::context_t* Context;
}

void zmq_initialize(int io_threads)
{
  if (detail::Context == nullptr)
  {
    std::lock_guard<std::mutex> lk(detail::Mutex);
    if (detail::Context == nullptr)
    {
      detail::Context = new zmq::context_t(io_threads);
    }
  }
}

void zmq_finalize()
{
  // FIXME: this deadlocks on macOS for some reason.
  // for demo, commenting it out and letting it LEAK!
  // delete detail::Context;
  // detail::Context = nullptr;
}

zmq::context_t& get_zmq_context()
{
  comm::zmq_initialize();
  return (*detail::Context);
}

Message recv(zmq::socket_t& endpoint)
{
  Message message;
  zmq::message_t aframe;
  int more = 1;
  size_t more_size = sizeof(more);
  while (more && endpoint.recv(&aframe))
  {
    endpoint.getsockopt(ZMQ_RCVMORE, &more, &more_size);
    // LOG_S(9) << "recv(" << aframe.size() << ") + " << (more ? "more" : "nomore");
    message.push_back(std::move(aframe));
  }

  return message;
}

bool send(Message&& frames, zmq::socket_t& endpoint, int flags)
{
  if (auto sz = frames.size())
  {
    for (auto& aframe : frames)
    {
      // TODO: if endpoint.getsockopt(ZMQ_LAST_ENDPOINT,...) indicates
      // that this is not an `inproc://` connection, any message frame that's a
      // VTK data object frame must be serialized.
      if (!endpoint.send(std::move(aframe), (--sz) ? ZMQ_SNDMORE | flags : 0 | flags))
      {
        return false;
      }
    }
  }
  return true;
}

} // namespace comm
