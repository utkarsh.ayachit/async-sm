TODOs
=====

1. `Service::Subscribe` should reuse subscription for same channel. Thus avoid
   creating multiple streams for same channel.

  - How can we know when all receiver channels are gone, that way we can
    unsubcribed. That should avoid receiving notifcations we no longer care
    about.

2. Since tracking "blockages" on client are going be critical, can we log the
   time it takes for any code on client? That'll help iron out the throttling
   points.

3. We need to add first class support to move "heavy data". A message can
   comprise of VTK data objects. In that case, we don't want to serialize the
   data unless it's being shipped to another process. How can we support it
   gracefully?

4. The rendering happening in "main-loop" is causing a lockup for the "first"
   render when new data shows up since the first render is not as fast as
   subsequent renders.

Serialization
==============

Serialization of data to be "shipped" is an important aspect that warrants a
separate discussion.

`comm::Message` represents the collection of message frames that can be
communicated. When the frame has `light-data`, it can be copied, transmitted
without regards to what type of endpoint the socket is connected to i.e. inproc
or tcp. For `heavy-data` such as vtkDataObject's, however, we want to send
pointers for inproc and serialize using readers/writers for tcp. Our API needs
to support that gracefully.

Interrupt
=========

* On any service that does blocking work, we have an object manager
  (OManager). The OManager keeps track of all objects remotely controlled.

* When any unsolicited message is received by the service, it is processed as
  follows:

  - On an arbitrary thread, `OManager::preview(seq_num, message)` is called.
    This method can preview the message even while the main thread for the
    service is busy doing blocking working. This method will return the message
    to forward further down the channel (or empty message to indicate the
    message has already been handled and replied to).

  - On the main service thread, `OManager::process(seq_num, message)` is called.
    This method can do blocking work.


* This 2 stage processing of messages with guarantees about execution thread is
  adequate to implement various interrupt, collapse and quality assurance
  requirements of our system.

* Pseudocode for OManager

```c++
Message OManager::preview(uint64_t seq_num, Message message)
{
  if `message` is "create":
    create SIProxy for the object and register it.
    reply success
    return empty()
  else
    find SIProxy for id in message
    return SIProxy::preview(seq_num, message)
}


void OManager::process(uint64_t seq_num, Message message)
{
  find SIProxy for id in message
  if `message` is "destroy":
    delete it
    reply success
  else
    SIProxy::process(seq_num, message)
}
```

* Let's see how preview/process can be used to collapse render requests.

```c++
Message SMRenderView::preview(uint64_t seq_num, Message message)
{
  if `message` is "render":
      this->RenderTag = seg_num
      return message
  ...
}

Message SMRenderView::process(uint64_t seq_num, Message message)
{
  if `message` is "render":
      if (this->RenderTag == seq_num) or
         (now - last_render_time) > 1.0/requested_frame_rate
         last_render_time = now
         this->Render()
      else
         update camera params etc, but
         ignore render request, a newer one is already queued.
  ....
}
```

* Let's s
